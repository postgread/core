use ::std::env;
use ::std::process::{Command, Output, Stdio};
use ::std::str;

pub struct TestEnv {
    path: String,
    pub pg_host: String,
    pub pg_port: u16,
}

const HELPER_SCRIPT_ENVAR: &'static str = "POSTGREAD_TEST_HELPER_SCRIPT";

impl TestEnv {
    pub fn new() -> Result<Self, String> {
        let path = env::var(HELPER_SCRIPT_ENVAR)
            .map_err(|e| format!("cannot read path from envar \"{}\": {}", HELPER_SCRIPT_ENVAR, e.to_string()))?;
        let (pg_host, pg_port) = Self::get_pg_host_port(&path)?;  // not only to cache, but also to check the script
        Ok(Self {
            path,
            pg_host,
            pg_port,
        })
    }

    pub fn start_forward(&self, target_host: &str, target_port: u16) -> Result<(), String> {
        let status = Command::new(&self.path)
            .arg("start_forward")
            .arg(target_host)
            .arg(&target_port.to_string())
            .status()
            .map_err(|e| e.to_string())?;
        if status.success() {
            Ok(())
        } else {
            Err(format!("exited with {}", status))
        }
    }

    pub fn stop_forward(&self) -> Result<(), String> {
        let status = Command::new(&self.path)
            .arg("stop_forward")
            .status()
            .map_err(|e| e.to_string())?;
        if status.success() {
            Ok(())
        } else {
            Err(format!("exited with {}", status))
        }
    }

    pub fn client_exec(&self, cmd: &[&str]) -> Result<Output, String> {
        Command::new(&self.path)
            .arg("client_exec")
            .args(cmd)
            .output()
            .map_err(|e| e.to_string())
    }

    fn get_pg_host_port(path: &str) -> Result<(String, u16), String> {
        let result = Command::new(&path)
            .arg("get_pg_host_port")
            .stderr(Stdio::inherit())
            .output()
            .map_err(|e| e.to_string())?;
        if ! result.status.success() {
            return Err(format!("exited with {}", result.status));
        }
        let output = str::from_utf8(&result.stdout)
            .map_err(|e| e.to_string())?;
        let (host, port) = output.split_once(':')
            .ok_or(format!("cannot split '{}' by :", output))?;
        let host = host.to_owned();
        let port = port.trim().parse::<u16>()
            .map_err(|e| format!("cannot parse '{}': {}", output, e))?;
        Ok((host, port))
    }
}
