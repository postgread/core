#! /usr/bin/env bash

set -e

verbose=
client_host=
client_port=
target_host=
target_port=
ssh_priv_key=
control_socket=

usage() {
    cat >&2 <<____
usage: $0 [-v] -c CLIENT_HOST -C CLIENT_PORT -t TARGET_HOST -T TARGET_PORT -k SSH_PRIV_KEY -s CONTROL_SOCKET [-- SSH_ARGS... [-- COMMAND [ARGS...]]]"
____
    exit 1
}

while getopts :vc:C:t:T:k:s: opt; do
    case $opt in
        v) verbose=yes ;;
        c) client_host=$OPTARG ;;
        C) client_port=$OPTARG ;;
        t) target_host=$OPTARG ;;
        T) target_port=$OPTARG ;;
        k) ssh_priv_key=$OPTARG ;;
        s) control_socket=$OPTARG ;;
        \?|:) usage ;;
  esac
done
shift $((OPTIND-1))

[[
    -n $client_host &&
    $client_port -gt 0 &&
    -n $target_host &&
    $target_port -gt 0 &&
    -f $ssh_priv_key &&
    -n $control_socket
]] || usage

ssh_args=()
while [[ $# -gt 0 ]] ; do
    if [[ $1 = '--' ]] ; then
        shift
        break
    else
        ssh_args=("${ssh_args[@]}" "$1")
        shift
    fi
done

cmd_args=("$@")

if [[ $verbose = yes ]] ; then
    set -x
else
    quiet_arg=-q
fi

exec ssh ${quiet_arg} \
    -o AddressFamily=inet \
    -o ControlMaster=auto \
    -o ControlPath="$control_socket" \
    -o ExitOnForwardFailure=yes \
    -o ForkAfterAuthentication=yes \
    -o IdentitiesOnly=yes \
    -o IdentityFile="$ssh_priv_key" \
    -o PasswordAuthentication=no \
    -o Port=$client_port \
    -o SessionType=none \
    -o StrictHostKeyChecking=no \
    -o UserKnownHostsFile=/dev/null \
    -R 5432:"$target_host":$target_port \
    "${ssh_args[@]}" \
    client@"$client_host" \
    "${cmd_args[@]}"
