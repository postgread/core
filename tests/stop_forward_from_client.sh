#! /usr/bin/env bash

set -e

verbose=
client_host=
client_port=
control_socket=

usage() {
    cat >&2 <<____
usage: $0 [-v] -c CLIENT_HOST -C CLIENT_PORT -s CONTROL_SOCKET"
____
    exit 1
}
    
while getopts :vc:C:s: opt; do
    case $opt in
        v) verbose=yes ;;
        c) client_host=$OPTARG ;;
        C) client_port=$OPTARG ;;
        s) control_socket=$OPTARG ;;
        \?|:) usage ;;
  esac
done
shift $((OPTIND-1))

[[ $# -eq 0 ]] || usage

[[
    -n $client_host &&
    $client_port -gt 0 &&
    -n $control_socket
]] || usage

if [[ $verbose = yes ]] ; then
    set -x
else
    quiet_arg=-q
fi

exec ssh ${quiet_arg} \
    -o Port=$client_port \
    -o ControlPath="$control_socket" \
    -O exit \
    client@$client_host
