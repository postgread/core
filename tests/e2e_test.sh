#! /usr/bin/env bash

set -e

publish_ip=127.0.0.1
user_tmp_dir=

usage() {
    cat >&2 <<____
usage: $0 [-v] [-i PUBLISH_IP] [-d TMP_DIR] [-- COMMAND [ARGS...]]

If COMMAND is set, it is called with additional envars, and then test environment is destroyed.
If COMMAND is omitted, test environment is kept alive and its details are printed out.

  -i PUBLISH_IP   sets IP where containers' ports are published ($publish_ip by default)
  -d TMP_DIR      sets existing directory to store helper script, ssh keys, etc. (instead of random temporary one)
____
    exit 1
}

while getopts :vi:d: opt; do
    case $opt in
        v) verbose=yes ;;
        i) publish_ip=$OPTARG ;;
        p) publish_port=$OPTARG ;;
        d) user_tmp_dir=$OPTARG ;;
        \?|:) usage ;;
  esac
done
shift $((OPTIND-1))
cmd=("$@")

[[ -z $publish_ip || $publish_ip =~ ^[0-9.]{7,15}$ ]] || usage
[[ -z $publish_port || $publish_port -gt 0 ]] || usage

if [[ -z $user_tmp_dir ]] ; then
    tmp_dir=$(mktemp --tmpdir --directory postgread-core.test.XXX) || {
        echo >&2 Cannot create temporary directory: exited with $?
        exit 2
    }
elif [[ -d $user_tmp_dir ]] ; then
    tmp_dir=$user_tmp_dir
    use_existing_ssh_keys=yes
else
    echo >&2 "'$user_tmp_dir' is not a directory"
    exit 1
fi
[[ $verbose = yes ]] && echo >&2 "Using temporary directory '$tmp_dir'"

pg_password=$(< /proc/sys/kernel/random/uuid) || {
    echo >&2 Cannot generate random password: exited with $?
    exit 2
}

ssh_priv_key="$tmp_dir/ssh_key"
ssh_pub_key="$ssh_priv_key".pub

ensure_ssh_keys() {
    if [[ $use_existing_ssh_keys = yes ]] ; then
        if [[ -f $ssh_priv_key && -f $ssh_pub_key ]] ; then
            echo "Will use existing SSH keys: '$ssh_priv_key' and '$ssh_pub_key'"
            return 0
        fi
        rm -vf "$ssh_priv_key" "$ssh_pub_key"
    fi
    ssh-keygen -q -t rsa -f "$ssh_priv_key" -N '' </dev/null || {
        echo >&2 Cannot generate SSH key: exited with $?
        exit 2
    }
}

ensure_ssh_keys

script_dir=$(dirname "$0")

run_cr() {
    local subdir="$1" ; shift
    "$script_dir/$subdir"/run_container.sh ${verbose:+-v} "$@" -- --detach
}

echo >&2 Running PG container...
pg_cr_id=$(run_cr pg_server -i $publish_ip -s "$pg_password") || {
    echo >&2 Cannot run PG container: exited with $?
    exit 3
}

echo >&2 Running client container...
client_cr_id=$(run_cr client -i $publish_ip -k "$ssh_pub_key") || {
    echo >&2 Cannot run client container: exited with $?
    exit 3
}

dump_cr_diag() {
    local cr_id=$1
    echo >&2 "Container logs:"
    docker logs $cr_id ||:
    echo >&2 "Container info:"
    docker inspect $cr_id ||:
}

get_port_or_status() {
    local cr_id=$1 port=$2
    local port_expr='index .NetworkSettings.Ports "'$port'/tcp" 0 "HostPort"'
    local status_expr='.State.Health.Status'
    local tpl='
        {{if eq "healthy" '$status_expr'}}
            {{'$port_expr'}}
        {{else}}
            {{'$status_expr'}}
        {{end}}
    '
    local output  # separate declaration to protect command exit code
    output=$(docker inspect $cr_id -f "$tpl") || {
        err=$?
        dump_cr_diag $cr_id ||:
        exit $err
    }
    xargs <<<"$output"
}

is_port() {
    [[ "$1" =~ ^[0-9]+$ ]]
}

for i in {180..0} ; do
    echo >&2 -n .
    pg_port=$(get_port_or_status $pg_cr_id 5432) || {
        echo >&2 Cannot get port/status of PG container: exited with $?
        exit 3
    }
    client_port=$(get_port_or_status $client_cr_id 22) || {
        echo >&2 Cannot get port/status of client container: exited with $?
        exit 3
    }
    is_port "$pg_port" && is_port "$client_port" && break
    sleep 1
done
echo >&2

if is_port "$pg_port" ; then
    echo >&2 PG container is healthy and listens on :$pg_port
else
    echo >&2 PG container is still "$pg_port"
    dump_cr_diag $pg_cr_id ||:
    exit 3
fi

if is_port "$client_port" ; then
    echo >&2 Client container is healthy and listens on :$client_port
else
    echo >&2 Client container is still "$client_port"
    dump_cr_diag $client_cr_id ||:
    exit 3
fi

mk_helper_script() {
    local file="$tmp_dir/helper.sh"
    cat > "$file" <<____ && chmod +x "$file" && echo "$file"
#! /usr/bin/env bash

pg_cr_id=$pg_cr_id
client_cr_id=$client_cr_id
publish_ip=$publish_ip
pg_port=$pg_port
client_port=$client_port
pg_password=$pg_password
script_dir=$(realpath "$script_dir")
ssh_priv_key=$(realpath "$ssh_priv_key")
ssh_control_socket=$(realpath "$tmp_dir")/ssh_socket

source "\$script_dir"/e2e_test_helper.sh
____
}

helper_script=$(mk_helper_script) || {
    echo >&2 "Cannot make helper script '$helper_script'"
    exit 2
}

test_expr='2+3'
test_awaited=$(($test_expr))

echo >&2 Testing port forwarding and command execution on client...
test_sh_res=$("$helper_script" client_exec ${verbose:+-v} sh -c "echo \$(($test_expr))") || {
    echo >&2 Cannot execute psql on client forwarding to PG: exited with $?
    dump_cr_diag $client_cr_id ||:
    exit 5
}
[[ $test_sh_res = $test_awaited ]] || {
    echo >&2 Cannot get awaited result from command running on client: "got '$test_sh_res'"
    exit 5
}
echo >&2 Got awaited result from commanid running on client

"$helper_script" start_forward ${verbose:+-v} $publish_ip $pg_port || {
    echo >&2 Cannot forward PG server port from client: exited with $?
    dump_cr_diag $client_cr_id ||:
    exit 5
}
echo >&2 Forwarded PG server port from client

test_psql_res=$("$helper_script" client_exec ${verbose:+-v} psql -tAc "select $test_expr") || {
    echo >&2 Cannot execute psql on client forwarding to PG: exited with $?
    exit 5
}
[[ $test_psql_res = $test_awaited ]] || {
    echo >&2 Cannot get awaited result from psql running on client forwarding to PG: "got '$test_psql_res'"
    exit 5
}
echo >&2 Got awaited result from psql running on client forwarding to PG server

"$helper_script" stop_forward ${verbose:+-v} || {
    echo >&2 Cannot stop forwarding port from client: exited with $?
    dump_cr_diag $client_cr_id ||:
    exit 5
}
echo >&2 Stopped forwarding port from client to PG server

if [[ ${#cmd[@]} -eq 0 ]] ; then
    echo "Helper script is '$helper_script' and contains:"
    cat "$helper_script"
else
    echo >&2 "Running ${cmd[@]@Q} in this test environment..."
    {
        env POSTGREAD_TEST_HELPER_SCRIPT="$helper_script" "${cmd[@]}"
        cmd_exit=$?
    } ||:
    echo >&2 "Command ${cmd[@]@Q} exited with $cmd_exit"
    docker stop $pg_cr_id >/dev/null || {
        echo >&2 Cannot stop PG container: exited with $?
        docker_stop_failed=yes
    }
    docker stop $client_cr_id >/dev/null || {
        echo >&2 Cannot stop client container: exited with $?
        docker_stop_failed=yes
    }
    rm -r "$tmp_dir" || {
	      echo >&2 "Cannot remove temporary directory '$tmp_dir'"
	      rm_tmp_failed=yes
    }
    [[ $cmd_exit -eq 0 ]] || exit 7
    [[ -z $docker_stop_failed ]] || exit 8
    [[ -z $rm_tmp_failed ]] || exit 9
    echo >&2 Cleaned everything up
fi
