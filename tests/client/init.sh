#! /bin/sh

set -e

echo "$SSH_PUB_KEY" > /etc/ssh/authorized_keys.d/client

exec /usr/sbin/sshd "$@"
