#! /usr/bin/env bash

set -e

verbose=
publish_ip=
publish_port=
ssh_pub_key=


usage() {
    cat >&2 <<____
usage: $0 [-v] -i PUBLISH_IP [-p PUBLISH_PORT] -k SSK_PUB_KEY [-- DOCKER_ARGS... [-- CONTAINER_ARGS...]]"
____
    exit 1
}

while getopts :vi:p:k: opt; do
    case $opt in
        v) verbose=yes ;;
        i) publish_ip=$OPTARG ;;
        p) publish_port=$OPTARG ;;
        k) ssh_pub_key=$OPTARG ;;
        \?|:) usage ;;
  esac
done
shift $((OPTIND-1))

[[ $publish_ip =~ ^[0-9.]{7,15}$ && -f $ssh_pub_key ]] || usage
[[ -z $publish_port || publish_port -gt 0 ]] || usage

docker_args=()
while [[ $# -gt 0 ]] ; do
    if [[ $1 = '--' ]] ; then
        shift
        break
    else
        docker_args=("${docker_args[@]}" "$1")
        shift
    fi
done

container_args=("$@")

[[ -f $ssh_pub_key ]] || {
    echo >&2 "SSH pub key '$ssh_pub_key' is missing or not a regular file"
    exit 2
}

tag=$(< "$(dirname "$0")"/tag)

[[ $verbose = yes ]] && set -x

exec docker run \
    --init \
    --publish $publish_ip:$publish_port:22 \
    --env SSH_PUB_KEY="$(< "$ssh_pub_key")" \
    --health-start-period 1s \
    --health-cmd 'netstat -n -t -l -p | grep -E " 0.0.0.0:22 .*/sshd"' \
    --health-timeout 500ms \
    --health-interval 1s \
    "${docker_args[@]}" \
    $tag \
    "${container_args[@]}"
