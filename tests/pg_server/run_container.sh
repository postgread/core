#! /usr/bin/env bash

set -e

verbose=
publish_ip=
publish_port=
pg_password=

usage() {
    cat >&2 <<____
usage: $0 [-v] -i PUBLISH_IP [-p PUBLISH_PORT] -s PG_PASSWORD [-- DOCKER_ARGS... [-- CONTAINER_ARGS...]]"
____
    exit 1
}

while getopts :vi:p:s: opt; do
    case $opt in
        v) verbose=yes ;;
        i) publish_ip=$OPTARG ;;
        p) publish_port=$OPTARG ;;
        s) pg_password=$OPTARG ;;
        \?|:) usage ;;
  esac
done
shift $((OPTIND-1))

[[ $publish_ip =~ ^[0-9.]{7,15}$ && -n $pg_password ]] || usage
[[ -z $publish_port || publish_port -gt 0 ]] || usage

docker_args=()
while [[ $# -gt 0 ]] ; do
    if [[ $1 = '--' ]] ; then
        shift
        break
    else
        docker_args=("${docker_args[@]}" "$1")
        shift
    fi
done

container_args=("$@")

tag=$(< "$(dirname "$0")"/tag)

[[ $verbose = yes ]] && set -x

exec docker run \
    --init \
    --publish "$publish_ip:$publish_port:5432" \
    --env "POSTGRES_PASSWORD=$pg_password" \
    --health-start-period 5s \
    --health-cmd 'pg_isready --username postgres --timeout=0' \
    --health-timeout 500ms \
    --health-interval 1s \
    "${docker_args[@]}" \
    "$tag" \
    "${container_args[@]}"
