mod global_fixture;
mod test_env;

#[macro_use] extern crate claim;
#[macro_use] extern crate lazy_static;
extern crate rstest;

use crate::global_fixture as gf;
use crate::test_env::TestEnv;

use postgread::convey::{Message, Side, Side::*, downcast_msg};
use postgread::msg::body::{*, initial::*};
use postgread::server::{self, Server};

use ::async_std::task;
use ::core::marker;
use ::core::str::from_utf8;
use ::rstest::*;
use ::std::env;
use ::std::io;
use ::std::net::Ipv4Addr;
use ::std::process::Output;
use ::std::sync::{Arc, Mutex};
use postgread::msg::util::decode::MsgDecode;

#[rstest]
async fn t1(fixture: Fixture) {
    let Output { status, stdout, stderr } = fixture.exec_on_client(&["psql"]).await.unwrap();
    assert!(status.success());
    assert_matches!(stdout.as_slice(), []);
    assert_matches!(stderr.as_slice(), []);
    let messages = fixture.messages.lock().unwrap();
    let mut m = messages.iter();
    assert_eq!(frontend::<Initial>(m.next()), &Initial::TLS);
    assert_eq!(frontend::<Initial>(m.next()), &Initial::Startup(Startup {
        version: Version { major: 3, minor: 0 },
        params: vec![
            StartupParam::new("user".into(), "postgres".into()),
            StartupParam::new("database".into(), "postgres".into()),
            StartupParam::new("application_name".into(), "psql".into()),
        ]
    }));
    assert_matches!(backend::<Authentication>(m.next()), &Authentication::Md5Password { salt: _ });
    assert_matches!(frontend::<Password>(m.next()), &Password(_));
    assert_eq!(backend::<Authentication>(m.next()), &Authentication::Ok);
    assert_eq!(backend::<ParameterStatus>(m.next()), &ParameterStatus::new("application_name".into(), "psql".into()));
    assert_eq!(backend::<ParameterStatus>(m.next()), &ParameterStatus::new("client_encoding".into(), "UTF8".into()));
    assert_eq!(backend::<ParameterStatus>(m.next()), &ParameterStatus::new("DateStyle".into(), "ISO, MDY".into()));
    assert_eq!(backend::<ParameterStatus>(m.next()), &ParameterStatus::new("integer_datetimes".into(), "on".into()));
    assert_eq!(backend::<ParameterStatus>(m.next()), &ParameterStatus::new("IntervalStyle".into(), "postgres".into()));
    assert_eq!(backend::<ParameterStatus>(m.next()), &ParameterStatus::new("is_superuser".into(), "on".into()));
    assert_eq!(backend::<ParameterStatus>(m.next()), &ParameterStatus::new("server_encoding".into(), "UTF8".into()));
    {
        let param = backend::<ParameterStatus>(m.next());
        assert_ok_eq!(from_utf8(&param.name), "server_version");
        assert!(assert_ok!(from_utf8(&param.value)).starts_with("12."));
    }
    assert_eq!(backend::<ParameterStatus>(m.next()), &ParameterStatus::new("session_authorization".into(), "postgres".into()));
    assert_eq!(backend::<ParameterStatus>(m.next()), &ParameterStatus::new("standard_conforming_strings".into(), "on".into()));
    assert_eq!(backend::<ParameterStatus>(m.next()), &ParameterStatus::new("TimeZone".into(), "UTC".into()));
    assert_matches!(backend::<BackendKeyData>(m.next()), &BackendKeyData { process_id: _, secret_key: _ });
    assert_eq!(backend::<ReadyForQuery>(m.next()), &ReadyForQuery { status: ready_for_query::Status::Idle });
    assert_eq!(frontend::<Terminate>(m.next()), &Terminate{});
    assert_none!(m.next());
}

type DynMessage = dyn Message + Send + marker::Sync + 'static;
type BoxedMessage = Box<DynMessage>;

fn backend<Msg: MsgDecode + 'static>(val: Option<&(Side, BoxedMessage)>) -> &Msg {
    assert_side_and_msg(Backend, val)
}

fn frontend<Msg: MsgDecode + 'static>(val: Option<&(Side, BoxedMessage)>) -> &Msg {
    assert_side_and_msg(Frontend, val)
}

fn assert_side_and_msg<Msg: MsgDecode + 'static>(expected_side: Side, val: Option<&(Side, BoxedMessage)>) -> &Msg {
    let (side, msg) = assert_some!(val);
    assert_eq!(expected_side, *side);
    assert_some!(downcast_msg::<Msg>(msg.as_ref()))
}

#[fixture]
fn fixture(bound_fixture: BoundFixture) -> Fixture {
    Fixture::new(bound_fixture)
}

struct Fixture {
    messages: Arc<Mutex<Vec<(Side, BoxedMessage)>>>,
    postgread_server_handle: Option<task::JoinHandle<io::Result<()>>>,
    test_env: Arc<TestEnv>,
}

impl Fixture {
    fn new(bound_fixture: BoundFixture) -> Self {
        let test_env = bound_fixture.test_context.test_env.clone();
        let listen_addr = Ipv4Addr::LOCALHOST;
        let server = task::block_on(start_server(listen_addr, &test_env.pg_host, test_env.pg_port))
            .expect("could not start postgread server");
        let server_port = server.get_listen_port()
            .expect("could not get port listened by postgread server");
        let messages = Arc::new(Mutex::new(vec![]));
        let messages2 = messages.clone();
        let server_handle = task::spawn(server::loop_accepting(
            server,
            Arc::new(move |side, msg: &DynMessage| {
                messages2.lock().unwrap().push((side, msg.clone_boxed()));
            })
        ));
        test_env.start_forward(&listen_addr.to_string(),server_port)
            .expect("could not forward port from client to postgread");
        Self {
            messages,
            postgread_server_handle: Some(server_handle),
            test_env,
        }
    }

    async fn exec_on_client(&self, cmd: &[&str]) -> Result<Output, String> {
        self.test_env.client_exec(cmd)
    }
}

impl Drop for Fixture {
    fn drop(&mut self) {
        let postgread_server_loop = std::mem::take(&mut self.postgread_server_handle)
            .expect("could not take server handle; it is strange");
        if let Some(early_exit) = task::block_on(postgread_server_loop.cancel()) {
            panic!("server exited earlier: {:?}", early_exit);
        }
        self.test_env.stop_forward()
            .expect("could not stop port forwarder (test client container -> postgread server");
    }
}

#[fixture]
fn bound_fixture() -> BoundFixture {
    BoundFixture::new()
}

async fn start_server(listen_addr: Ipv4Addr, pg_server_host: &str, pg_server_port: u16) -> Result<Server, String> {
    let config = server::Config {
        listen_addr: listen_addr.into(),
        listen_port: 0,
        target_host: pg_server_host.to_owned(),
        target_port: pg_server_port,
        cert_p12_file: concat!(env!("CARGO_MANIFEST_DIR"), "/try/cert.p12").to_owned(),
        cert_p12_password: "".to_owned(),
    };
    server::listen(config).await.map_err(|e| e.to_string())
}

type BoundFixture = gf::BoundFixture<GlobalFixture>;

struct GlobalFixture();

impl gf::GlobalFixture for GlobalFixture {
    fn setup() -> Result<(Self::TestContext, Self::TearDownHandle), String> {
        let test_env = TestEnv::new()
            .map_err(|e| format!("could not setup test env: {}", e))?;
        let context = Self::TestContext {
            test_env: Arc::new(test_env)
        };
        let tear_down = ();
        Ok((context, tear_down))
    }

    fn tear_down(_: Self::TearDownHandle) -> Result<(), String> {
        Ok(())
    }

    type TestContext = TestContext;
    type TearDownHandle = ();

    fn get_mutex_context() -> &'static gf::GlobalMutexContext<Self::TestContext, Self::TearDownHandle> {
        &SHARED_CONTEXT
    }

    const TESTS_FILE_CONTENT: &'static str = include_str!(concat!(env!("CARGO_MANIFEST_DIR"), "/", file!()));
}

#[derive(Clone)]
struct TestContext {
    test_env: Arc<TestEnv>,
}

lazy_static! {
    static ref SHARED_CONTEXT: gf::GlobalMutexContext<TestContext, ()> = gf::new_global_mutex_context();
}
