#! /usr/bin/env bash

usage() {
    cat >&2 <<____
usage: $0 start_forward [-v] TARGET_HOST TARGET_PORT
   or: $0 stop_forward [-v]
   or: $0 client_exec [-v] COMMAND [ARGS...]
   or: $0 get_pg_host_port
____
    exit 91
}

[[
    $client_cr_id =~ ^[0-9a-f]+{12,}$ &&
    $publish_ip =~ ^[0-9.]{7,15}$ &&
    $pg_port -gt 0 &&
    $client_port -gt 0 &&
    -n $pg_password &&
    -d $script_dir &&
    -f $ssh_priv_key &&
    -n $ssh_control_socket
]] || {
    echo >&2 "This file is supposed to be included from temporarily generated script which should define variables."
    ( set -x ; cat >&2 "$0" )
    exit 92
}

action="$1" ; shift

if [[ $1 = '-v' ]] ; then
    verbose=yes
    shift
fi

case "$action" in
    start_forward)
        target_host=$1
        target_port=$2
        [[ -n $target_host && $target_port -gt 0 ]] || usage
        exec timeout -v 3s "$script_dir"/start_forward_from_client.sh \
            ${verbose:+-v} \
            -c $publish_ip \
            -C $client_port \
            -t "$target_host" \
            -T $target_port \
            -k "$ssh_priv_key" \
            -s "$ssh_control_socket"
        ;;
    stop_forward)
        exec timeout -v 3s "$script_dir"/stop_forward_from_client.sh \
            ${verbose:+-v} \
            -c $publish_ip \
            -C $client_port \
            -s "$ssh_control_socket"
        ;;
    client_exec)
        cmd=("$@")
        [[ $verbose = yes ]] && set -x
        exec docker exec \
            -e PGHOST=127.0.0.1 \
            -e PGUSER=postgres \
            -e PGPASSWORD="$pg_password" \
            $client_cr_id \
            "${cmd[@]}"
        ;;
    get_pg_host_port)
        echo $publish_ip:$pg_port
        ;;
    *)
        usage
        ;;
esac
