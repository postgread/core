use async_trait::async_trait;
use futures::{AsyncWrite, AsyncWriteExt};
use std::io::Result as IoResult;

#[async_trait]
pub trait Writer: Send + Unpin {
    async fn write_bytes(&mut self, bytes: &[u8]) -> IoResult<()>;
}

#[async_trait]
impl<W> Writer for W
where W: AsyncWrite + Send + Unpin {
    async fn write_bytes(&mut self, bytes: &[u8]) -> IoResult<()> {
        self.write_all(bytes).await
    }
}
