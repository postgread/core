use crate::convey::reader::Reader;
use crate::convey::writer::Writer;
use crate::msg::util::decode::MsgDecode;
use crate::msg::util::read::ReadResult;

use ::std::error::Error as StdError;
use ::std::future::Future;
use ::std::hint::unreachable_unchecked;
use ::std::io::Result as IoResult;

pub enum Tlsable<Plain, Tls> {
    Plain(Plain),
    TlsHandshake,
    Tls(Tls),
}

#[derive(Debug)]
pub enum TlsError {
    IllegalHandshakeState,
    HandshakeImplError(String),
}

pub type TlsResult<T> = Result<T, TlsError>;

use Tlsable::*;
use TlsError::*;

impl<Plain, Tls> Tlsable<Plain, Tls>
where Plain: Send + Unpin
{
    pub async fn establish_tls
        <FnHandshake, FutHandshake, TlsImplError>
        (&mut self, fn_handshake: FnHandshake) -> TlsResult<()>
    where
        FnHandshake: FnOnce(Plain) -> FutHandshake,
        FutHandshake: Future<Output=Result<Tls, TlsImplError>>,
        TlsImplError: StdError,
    {
        let plain = self.switch_to_handshake().ok_or(IllegalHandshakeState)?;
        let tls = fn_handshake(plain).await
            .map_err(|e| HandshakeImplError(e.to_string()))?;
        if self.switch_to_tls(tls) {
            Ok(())
        } else {
            Err(IllegalHandshakeState)
        }
    }

    fn switch_to_handshake(&mut self) -> Option<Plain> {
        match self {
            Plain(_) => match core::mem::replace(self, TlsHandshake) {
                Plain(plain) => Some(plain),
                _ => unsafe { unreachable_unchecked() },
            }
            _ => None
        }
    }

    fn switch_to_tls(&mut self, tls: Tls) -> bool {
        match self {
            TlsHandshake => match core::mem::replace(self, Tls(tls)) {
                TlsHandshake => true,
                _ => unsafe { unreachable_unchecked() },
            }
            _ => false
        }
    }
}

impl<Plain, Tls> Tlsable<Plain, Tls>
where
    Plain: Reader,
    Tls: Reader,
{
    pub async fn read_type_byte(&mut self) -> TlsResult<IoResult<u8>> {
        match self {
            Plain(ref mut plain) => Ok(plain.read_type_byte().await),
            Tls(ref mut tls) => Ok(tls.read_type_byte().await),
            TlsHandshake => Err(IllegalHandshakeState),
        }
    }

    pub async fn read_msg<Msg>(&mut self) -> TlsResult<ReadResult<Msg>>
    where Msg: 'static + MsgDecode
    {
        match self {
            Plain(ref mut plain) => Ok(plain.read_msg().await),
            Tls(ref mut tls) => Ok(tls.read_msg().await),
            TlsHandshake => Err(IllegalHandshakeState),
        }
    }
}

impl<Plain, Tls> Tlsable<Plain, Tls>
where
    Plain: Writer,
    Tls: Writer,
{
    pub async fn write(&mut self, bytes: &[u8]) -> TlsResult<IoResult<()>> {
        let wr = self.as_convey_writer()?;
        Ok(wr.write_bytes(bytes).await)
    }

    fn as_convey_writer(&mut self) -> Result<&mut dyn Writer, TlsError> {
        match self {
            Plain(ref mut plain) => Ok(plain),
            Tls(ref mut tls) => Ok(tls),
            TlsHandshake => Err(IllegalHandshakeState),
        }
    }
}
