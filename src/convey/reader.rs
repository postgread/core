use async_trait::async_trait;
use futures::AsyncRead;
use std::io::Result as IoResult;
use crate::msg::util::async_io;
use crate::msg::util::decode::MsgDecode;
use crate::msg::util::read::{read_msg, ReadResult};

#[async_trait]
pub trait Reader: Send + Unpin {
    async fn read_msg<Msg>(&mut self) -> ReadResult<Msg>
    where Msg: 'static + MsgDecode;

    async fn read_type_byte(&mut self) -> IoResult<u8>;
}

#[async_trait]
impl<R> Reader for R
where R: AsyncRead + Send + Unpin {
    async fn read_msg<Msg>(&mut self) -> ReadResult<Msg>
    where Msg: 'static + MsgDecode {
        read_msg(self).await
    }

    async fn read_type_byte(&mut self) -> IoResult<u8> {
        async_io::read_u8(self).await
    }
}
