use crate::convey::{downcast_msg, Message, Side, Side::*, TLS_NOT_SUPPORTED, TLS_SUPPORTED};
use crate::convey::reader::Reader;
use crate::convey::tests::fake_tls::{FakeTlsStream, FakeTlsUnderneath};
use crate::convey::writer::Writer;
use crate::msg::util::decode::MsgDecode;
use crate::msg::util::read::{ReadData, ReadError, ReadResult};

use ::async_trait::async_trait;
use ::core::fmt::{self, Debug, Formatter};
use ::core::pin::Pin;
use ::core::sync::atomic::{AtomicUsize, Ordering::Relaxed};
use ::std::io::{Error as IoError, ErrorKind, Result as IoResult};
use ::std::sync::Arc;
use ::futures::{Stream, StreamExt};
use ::futures::task::{Context, Poll, Poll::*};

pub struct TwoFakeStreams {
    items: Vec<(Side, FakeDataForm)>,
    backend_tls_started: bool,
    frontend_tls_started: bool,
}

pub struct FakeStream<'a> {
    side: Side,
    items: &'a Vec<(Side, FakeDataForm)>,
    cursor: Arc<AtomicUsize>,
    is_tls: bool,
}

#[derive(Debug)]
pub struct FakeDataForm {
    is_tls: bool,
    pub data: FakeData,
}

pub enum FakeData {
    TypeByte(u8),
    Body(Box<dyn Message + Send + Sync>),
}

use FakeData::*;

impl TwoFakeStreams {
    pub fn new() -> Self {
        Self {
            items: vec![],
            backend_tls_started: false,
            frontend_tls_started: false,
        }
    }

    pub fn backend_accepts_tls(&mut self) {
        assert!(!self.backend_tls_started);
        self.push_type_byte(Backend, false, TLS_SUPPORTED);
        self.backend_tls_started = true;
    }

    pub fn backend_rejects_tls(&mut self) {
        assert!(!self.backend_tls_started);
        self.push_type_byte(Backend, false, TLS_NOT_SUPPORTED);
    }

    pub fn frontend_starts_tls(&mut self) {
        assert!(!self.frontend_tls_started);
        self.frontend_tls_started = true;
    }

    pub fn backend_sends<Msg>(&mut self, msg: Msg)
    where Msg: 'static + MsgDecode + Send + Sync {
        self.push(Backend, self.backend_tls_started, msg)
    }

    pub fn frontend_sends<Msg>(&mut self, msg: Msg)
    where Msg: 'static + MsgDecode + Send + Sync {
        self.push(Frontend, self.frontend_tls_started, msg)
    }

    pub fn split_backend_frontend(&self) -> TwoFakeStreamsSplit {
        let cursor = Arc::new(AtomicUsize::new(0));
        TwoFakeStreamsSplit {
            cursor,
            items: &self.items,
        }
    }

    fn push<Msg>(&mut self, side: Side, is_tls: bool, msg: Msg)
    where Msg: 'static + MsgDecode + Send + Sync {
        if let Some(type_byte) = Msg::TYPE_BYTE_OPT {
            self.push_type_byte(side, is_tls, type_byte.into())
        }
        let body: Box<dyn Message + Send + Sync> = Box::new(msg);
        self.items.push((side, FakeDataForm::from_body(is_tls, body)))
    }

    fn push_type_byte(&mut self, side: Side, is_tls: bool, byte: u8) {
        self.items.push((side, FakeDataForm::from_type_byte(is_tls, byte)))
    }
}

pub struct TwoFakeStreamsSplit<'a> {
    items: &'a Vec<(Side, FakeDataForm)>,
    cursor: Arc<AtomicUsize>,
}

impl TwoFakeStreamsSplit<'_> {
    pub fn backend(&self) -> FakeStream<'_> {
        FakeStream::new(&self.items, self.cursor.clone(), Backend)
    }

    pub fn frontend(&self) -> FakeStream<'_> {
        FakeStream::new(&self.items, self.cursor.clone(), Frontend)
    }

    pub fn filter_messages(&self) -> impl Iterator<Item=(Side, &(dyn Message + Send + Sync))> {
        self.items.iter().filter_map(peek_body)
    }

    pub fn untaken(&self) -> impl Iterator<Item=&(Side, FakeDataForm)> {
        self.items[self.cursor.load(Relaxed)..].iter()
    }
}

fn peek_body((side, form): &(Side, FakeDataForm)) -> Option<(Side, &(dyn Message + Send + Sync))> {
    match &form.data {
        TypeByte(_) => None,
        Body(body) => Some((*side, body.as_ref())),
    }
}

#[async_trait]
impl<'a> Reader for FakeStream<'a> {
    async fn read_msg<Msg>(&mut self) -> ReadResult<Msg>
    where Msg: 'static + MsgDecode {
        let data = self.fake_read_data().await
            .map_err(ReadError::IoError)?;
        if let Body(msg) = data {
            let typed_msg: &Msg = downcast_msg::<Msg>(msg.as_ref())
                .ok_or(Self::make_read_err(&format!("cannot cast {:?} to Msg/{:?}", msg, Msg::TYPE_BYTE_OPT)))?;
            let msg_clone = (*typed_msg).clone();
            Ok(ReadData {
                bytes: vec![],
                msg_result: Ok(msg_clone),
            })
        } else {
            Err(Self::make_read_err(&format!("expected a message but found {:?}", data)))
        }
    }

    async fn read_type_byte(&mut self) -> IoResult<u8> {
        let data = self.fake_read_data().await?;
        if let TypeByte(type_byte) = data {
            Ok(*type_byte)
        } else {
            Err(Self::make_io_err(&format!("expected a typebyte but found {:?}", data)))
        }
    }
}

#[async_trait]
impl<'a> Reader for FakeTlsStream<FakeStream<'a>> {
    async fn read_msg<Msg>(&mut self) -> ReadResult<Msg>
    where Msg: 'static + MsgDecode {
        self.plain.read_msg().await
    }

    async fn read_type_byte(&mut self) -> IoResult<u8> {
        self.plain.read_type_byte().await
    }
}


#[async_trait]
impl<'a> Writer for FakeStream<'a> {
    async fn write_bytes(&mut self, _bytes: &[u8]) -> IoResult<()> {
        Ok(())  // Just ignore the bytes for now
    }
}

#[async_trait]
impl<'a> Writer for FakeTlsStream<FakeStream<'a>> {
    async fn write_bytes(&mut self, bytes: &[u8]) -> IoResult<()> {
        self.plain.write_bytes(bytes).await
    }
}

impl<'a> FakeStream<'a> {
    pub fn new(items: &'a Vec<(Side, FakeDataForm)>, cursor: Arc<AtomicUsize>, filtering_side: Side) -> Self {
        Self {
            side: filtering_side,
            items,
            cursor,
            is_tls: false,
        }
    }

    async fn fake_read_data(&mut self) -> IoResult<&'a FakeData> {
        let FakeDataForm { is_tls, data } = self.next().await
            .ok_or(Self::make_io_eof())?;
        if *is_tls != self.is_tls {
            let err = if *is_tls {
                "the packet is TLS-encrypted while the stream isn't"
            } else {
                "the stream is TLS-encrypted while the packet isn't"
            };
            return Err(Self::make_io_err(err))
        }
        Ok(data)
    }

    fn make_read_err(text: &str) -> ReadError {
        ReadError::IoError(Self::make_io_err(text))
    }

    fn make_io_eof() -> IoError {
        IoError::from(ErrorKind::UnexpectedEof)
    }

    fn make_io_err(text: &str) -> IoError {
        IoError::new(ErrorKind::Other, text)
    }
}

impl<'a> Stream for FakeStream<'a> {
    type Item = &'a FakeDataForm;

    fn poll_next(self: Pin<&mut Self>, _: &mut Context) -> Poll<Option<Self::Item>> {
        let cursor = self.cursor.load(Relaxed);
        if cursor >= self.items.len() {
            return Ready(None)
        }
        let (side, data) = &self.items[cursor];
        if *side == self.side {
            self.cursor.fetch_add(1, Relaxed);
            Ready(Some(data))
        } else {
            Pending
        }
    }
}

impl<'a> FakeTlsUnderneath for FakeStream<'a> {
    fn switch_to_tls(&mut self) {
        self.is_tls = true;
    }
}

impl FakeDataForm {
    const fn from_type_byte(is_tls: bool, byte: u8) -> Self {
        Self {
            is_tls,
            data: TypeByte(byte),
        }
    }

    const fn from_body(is_tls: bool, body: Box<dyn Message + Send + Sync>) -> Self {
        Self {
            is_tls,
            data: Body(body),
        }
    }
}

impl Debug for FakeData {
    fn fmt(&self, f: &mut Formatter) -> Result<(), fmt::Error> {
        match self {
            TypeByte(type_byte) => write!(f, "TypeByte('{}'={})", *type_byte as char, type_byte),
            Body(msg) => write!(f, "Body<{:?}>", msg),
        }
    }
}
