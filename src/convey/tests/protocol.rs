use super::fake_stream::TwoFakeStreams;
use super::fake_tls::*;
use super::new_msg::*;

use crate::convey::{Conveyor, ConveyResult, Error::*, Message, downcast_msg};

use ::async_std::task;
use ::core::fmt::Debug;

// https://www.postgresql.org/docs/12/protocol.html

#[test]
fn cancel() {
    let mut x = TwoFakeStreams::new();
    x.frontend_sends(initial::cancel(11, 12));
    assert_ok!(test_convey(x));
}

#[test]
fn cancel_when_backend_accepts_tls() {
    let mut x = TwoFakeStreams::new();
    x.frontend_sends(initial::tls);
    x.frontend_starts_tls();
    x.backend_accepts_tls();
    x.frontend_sends(initial::cancel(11, 12));
    assert_ok!(test_convey(x));
}

#[test]
fn cancel_when_backend_rejects_tls() {
    let mut x = TwoFakeStreams::new();
    x.frontend_sends(initial::tls);
    x.frontend_starts_tls();
    x.backend_rejects_tls();
    x.frontend_sends(initial::cancel(11, 12));
    assert_ok!(test_convey(x));
}

#[test]
fn backend_does_not_know_tls() {
    let mut x = TwoFakeStreams::new();
    x.frontend_sends(initial::tls);
    x.backend_sends(error_response("too old backend"));
    assert_ok!(test_convey(x));
}

#[test]
fn error_after_startup() {
    let mut x = TwoFakeStreams::new();
    x.frontend_sends(initial::startup(11, 12, hashmap!{}));
    x.backend_sends(error_response("something goes wrong"));
    assert_ok!(test_convey(x));
}

#[test]
fn negotiate_and_error_after_startup() {
    let mut x = TwoFakeStreams::new();
    x.frontend_sends(initial::startup(11, 12, hashmap!{}));
    x.backend_sends(negotiate_protocol_version(21, &["_pq_x", "_pq_y"]));
    x.backend_sends(error_response("something goes wrong"));
    assert_ok!(test_convey(x));
}

#[test]
fn error_after_startup_when_backend_accepts_tls() {
    let mut x = TwoFakeStreams::new();
    x.frontend_sends(initial::tls);
    x.frontend_starts_tls();
    x.backend_accepts_tls();
    x.frontend_sends(initial::startup(11, 12, hashmap!{}));
    x.backend_sends(error_response("something goes wrong"));
    assert_ok!(test_convey(x));
}

#[test]
fn error_after_startup_when_backend_rejects_tls() {
    let mut x = TwoFakeStreams::new();
    x.frontend_sends(initial::tls);
    x.backend_rejects_tls();
    x.frontend_starts_tls();
    x.frontend_sends(initial::startup(11, 12, hashmap!{}));
    x.backend_sends(error_response("something goes wrong"));
    assert_ok!(test_convey(x));
}

#[test]
fn auth_ok_and_error() {
    let mut x = TwoFakeStreams::new();
    x.frontend_sends(initial::startup(11, 12, hashmap!{}));
    x.backend_sends(authentication::ok);
    x.backend_sends(error_response("something goes wrong"));
    assert_ok!(test_convey(x));
}

#[test]
fn auth_cleartext_with_correct_password() {
    let mut x = TwoFakeStreams::new();
    x.frontend_sends(initial::startup(11, 12, hashmap!{}));
    x.backend_sends(authentication::cleartext_password);
    x.frontend_sends(password("correct"));
    x.backend_sends(authentication::ok);
    x.backend_sends(error_response("shorten test"));
    assert_ok!(test_convey(x));
}

#[test]
fn auth_cleartext_with_wrong_password() {
    let mut x = TwoFakeStreams::new();
    x.frontend_sends(initial::startup(11, 12, hashmap!{}));
    x.backend_sends(authentication::cleartext_password);
    x.frontend_sends(password("wrong"));
    x.backend_sends(error_response("wrong password"));
    assert_ok!(test_convey(x));
}

#[test]
fn auth_md5_with_correct_password() {
    let mut x = TwoFakeStreams::new();
    x.frontend_sends(initial::startup(11, 12, hashmap!{}));
    x.backend_sends(authentication::md5_password(b"salt"));
    x.frontend_sends(password("md5correct"));
    x.backend_sends(authentication::ok);
    x.backend_sends(error_response("shorten test"));
    assert_ok!(test_convey(x));
}

#[test]
fn auth_md5_with_wrong_password() {
    let mut x = TwoFakeStreams::new();
    x.frontend_sends(initial::startup(11, 12, hashmap!{}));
    x.backend_sends(authentication::md5_password(b"salt"));
    x.frontend_sends(password("md5wrong"));
    x.backend_sends(error_response("wrong password"));
    assert_ok!(test_convey(x));
}

#[test]
fn auth_gss_single_ok() {
    let mut x = TwoFakeStreams::new();
    x.frontend_sends(initial::startup(11, 12, hashmap!{}));
    x.backend_sends(authentication::gss);
    x.frontend_sends(gss_response(&[0xf1]));
    x.backend_sends(authentication::ok);
    x.backend_sends(error_response("shorten test"));
    assert_ok!(test_convey(x));
}

#[test]
fn auth_gss_single_error() {
    let mut x = TwoFakeStreams::new();
    x.frontend_sends(initial::startup(11, 12, hashmap!{}));
    x.backend_sends(authentication::gss);
    x.frontend_sends(gss_response(&[0xf1]));
    x.backend_sends(error_response("something wrong"));
    assert_ok!(test_convey(x));
}

#[test]
fn auth_gss_double_ok() {
    let mut x = TwoFakeStreams::new();
    x.frontend_sends(initial::startup(11, 12, hashmap!{}));
    x.backend_sends(authentication::gss);
    x.frontend_sends(gss_response(&[0xf1]));
    x.backend_sends(authentication::gss_continue(&[0xb2]));
    x.frontend_sends(gss_response(&[0xf2]));
    x.backend_sends(authentication::ok);
    x.backend_sends(error_response("shorten test"));
    assert_ok!(test_convey(x));
}

#[test]
fn auth_gss_double_error() {
    let mut x = TwoFakeStreams::new();
    x.frontend_sends(initial::startup(11, 12, hashmap!{}));
    x.backend_sends(authentication::gss);
    x.frontend_sends(gss_response(&[0xf1]));
    x.backend_sends(authentication::gss_continue(&[0xb2]));
    x.frontend_sends(gss_response(&[0xf2]));
    x.backend_sends(error_response("something wrong"));
    assert_ok!(test_convey(x));
}

#[test]
fn auth_gss_quadruple_ok() {
    let mut x = TwoFakeStreams::new();
    x.frontend_sends(initial::startup(11, 12, hashmap!{}));
    x.backend_sends(authentication::gss);
    x.frontend_sends(gss_response(&[0xf1]));
    x.backend_sends(authentication::gss_continue(&[0xb2]));
    x.frontend_sends(gss_response(&[0xf2]));
    x.backend_sends(authentication::gss_continue(&[0xb3]));
    x.frontend_sends(gss_response(&[0xf3]));
    x.backend_sends(authentication::gss_continue(&[0xb4]));
    x.frontend_sends(gss_response(&[0xf4]));
    x.backend_sends(authentication::ok);
    x.backend_sends(error_response("shorten test"));
    assert_ok!(test_convey(x));
}

#[test]
fn auth_gss_quadruple_error() {
    let mut x = TwoFakeStreams::new();
    x.frontend_sends(initial::startup(11, 12, hashmap!{}));
    x.backend_sends(authentication::gss);
    x.frontend_sends(gss_response(&[0xf1]));
    x.backend_sends(authentication::gss_continue(&[0xb2]));
    x.frontend_sends(gss_response(&[0xf2]));
    x.backend_sends(authentication::gss_continue(&[0xb3]));
    x.frontend_sends(gss_response(&[0xf3]));
    x.backend_sends(authentication::gss_continue(&[0xb4]));
    x.frontend_sends(gss_response(&[0xf4]));
    x.backend_sends(error_response("something wrong"));
    assert_ok!(test_convey(x));
}

#[test]
fn auth_sspi_single_ok() {
    let mut x = TwoFakeStreams::new();
    x.frontend_sends(initial::startup(11, 12, hashmap!{}));
    x.backend_sends(authentication::sspi);
    x.frontend_sends(gss_response(&[0xf1]));
    x.backend_sends(authentication::ok);
    x.backend_sends(error_response("shorten test"));
    assert_ok!(test_convey(x));
}

#[test]
fn auth_sspi_single_error() {
    let mut x = TwoFakeStreams::new();
    x.frontend_sends(initial::startup(11, 12, hashmap!{}));
    x.backend_sends(authentication::sspi);
    x.frontend_sends(gss_response(&[0xf1]));
    x.backend_sends(error_response("something wrong"));
    assert_ok!(test_convey(x));
}

#[test]
fn auth_sspi_double_ok() {
    let mut x = TwoFakeStreams::new();
    x.frontend_sends(initial::startup(11, 12, hashmap!{}));
    x.backend_sends(authentication::sspi);
    x.frontend_sends(gss_response(&[0xf1]));
    x.backend_sends(authentication::gss_continue(&[0xb2]));
    x.frontend_sends(gss_response(&[0xf2]));
    x.backend_sends(authentication::ok);
    x.backend_sends(error_response("shorten test"));
    assert_ok!(test_convey(x));
}

#[test]
fn auth_sspi_double_error() {
    let mut x = TwoFakeStreams::new();
    x.frontend_sends(initial::startup(11, 12, hashmap!{}));
    x.backend_sends(authentication::sspi);
    x.frontend_sends(gss_response(&[0xf1]));
    x.backend_sends(authentication::gss_continue(&[0xb2]));
    x.frontend_sends(gss_response(&[0xf2]));
    x.backend_sends(error_response("something wrong"));
    assert_ok!(test_convey(x));
}

#[test]
fn auth_sspi_quadruple_ok() {
    let mut x = TwoFakeStreams::new();
    x.frontend_sends(initial::startup(11, 12, hashmap!{}));
    x.backend_sends(authentication::sspi);
    x.frontend_sends(gss_response(&[0xf1]));
    x.backend_sends(authentication::gss_continue(&[0xb2]));
    x.frontend_sends(gss_response(&[0xf2]));
    x.backend_sends(authentication::gss_continue(&[0xb3]));
    x.frontend_sends(gss_response(&[0xf3]));
    x.backend_sends(authentication::gss_continue(&[0xb4]));
    x.frontend_sends(gss_response(&[0xf4]));
    x.backend_sends(authentication::ok);
    x.backend_sends(error_response("shorten test"));
    assert_ok!(test_convey(x));
}

#[test]
fn auth_sspi_quadruple_error() {
    let mut x = TwoFakeStreams::new();
    x.frontend_sends(initial::startup(11, 12, hashmap!{}));
    x.backend_sends(authentication::sspi);
    x.frontend_sends(gss_response(&[0xf1]));
    x.backend_sends(authentication::gss_continue(&[0xb2]));
    x.frontend_sends(gss_response(&[0xf2]));
    x.backend_sends(authentication::gss_continue(&[0xb3]));
    x.frontend_sends(gss_response(&[0xf3]));
    x.backend_sends(authentication::gss_continue(&[0xb4]));
    x.frontend_sends(gss_response(&[0xf4]));
    x.backend_sends(error_response("something wrong"));
    assert_ok!(test_convey(x));
}

#[test]
fn auth_sasl_single_ok() {
    let mut x = TwoFakeStreams::new();
    x.frontend_sends(initial::startup(11, 12, hashmap!{}));
    x.backend_sends(authentication::sasl(&["a", "b"]));
    x.frontend_sends(sasl_initial_response("a"));
    x.backend_sends(authentication::sasl_final(""));
    x.backend_sends(authentication::ok);
    x.backend_sends(error_response("shorten test"));
    assert_ok!(test_convey(x));
}

#[test]
fn auth_sasl_single_error_after_final() {
    let mut x = TwoFakeStreams::new();
    x.frontend_sends(initial::startup(11, 12, hashmap!{}));
    x.backend_sends(authentication::sasl(&["a", "b"]));
    x.frontend_sends(sasl_initial_response("a"));
    x.backend_sends(authentication::sasl_final(""));
    x.backend_sends(error_response("something wrong"));
    assert_ok!(test_convey(x));
}

#[test]
fn auth_sasl_single_error() {
    let mut x = TwoFakeStreams::new();
    x.frontend_sends(initial::startup(11, 12, hashmap!{}));
    x.backend_sends(authentication::sasl(&["a", "b"]));
    x.frontend_sends(sasl_initial_response("a"));
    x.backend_sends(error_response("something wrong"));
    assert_ok!(test_convey(x));
}

#[test]
fn auth_sasl_double_ok() {
    let mut x = TwoFakeStreams::new();
    x.frontend_sends(initial::startup(11, 12, hashmap!{}));
    x.backend_sends(authentication::sasl(&["a", "b"]));
    x.frontend_sends(sasl_initial_response("a"));
    x.backend_sends(authentication::sasl_continue(""));
    x.frontend_sends(sasl_response(""));
    x.backend_sends(authentication::sasl_final(""));
    x.backend_sends(authentication::ok);
    x.backend_sends(error_response("shorten test"));
    assert_ok!(test_convey(x));
}

#[test]
fn auth_sasl_double_error_after_final() {
    let mut x = TwoFakeStreams::new();
    x.frontend_sends(initial::startup(11, 12, hashmap!{}));
    x.backend_sends(authentication::sasl(&["a", "b"]));
    x.frontend_sends(sasl_initial_response("a"));
    x.backend_sends(authentication::sasl_continue(""));
    x.frontend_sends(sasl_response(""));
    x.backend_sends(authentication::sasl_final(""));
    x.backend_sends(error_response("something wrong"));
    assert_ok!(test_convey(x));
}

#[test]
fn auth_sasl_double_error() {
    let mut x = TwoFakeStreams::new();
    x.frontend_sends(initial::startup(11, 12, hashmap!{}));
    x.backend_sends(authentication::sasl(&["a", "b"]));
    x.frontend_sends(sasl_initial_response("a"));
    x.backend_sends(authentication::sasl_continue(""));
    x.frontend_sends(sasl_response(""));
    x.backend_sends(error_response("something wrong"));
    assert_ok!(test_convey(x));
}

#[test]
fn auth_sasl_quadriple_ok() {
    let mut x = TwoFakeStreams::new();
    x.frontend_sends(initial::startup(11, 12, hashmap!{}));
    x.backend_sends(authentication::sasl(&["a", "b"]));
    x.frontend_sends(sasl_initial_response("a"));
    x.backend_sends(authentication::sasl_continue("2"));
    x.frontend_sends(sasl_response("2"));
    x.backend_sends(authentication::sasl_continue("3"));
    x.frontend_sends(sasl_response("3"));
    x.backend_sends(authentication::sasl_continue("4"));
    x.frontend_sends(sasl_response("4"));
    x.backend_sends(authentication::sasl_final(""));
    x.backend_sends(authentication::ok);
    x.backend_sends(error_response("shorten test"));
    assert_ok!(test_convey(x));
}

#[test]
fn auth_sasl_quadriple_error_after_final() {
    let mut x = TwoFakeStreams::new();
    x.frontend_sends(initial::startup(11, 12, hashmap!{}));
    x.backend_sends(authentication::sasl(&["a", "b"]));
    x.frontend_sends(sasl_initial_response("a"));
    x.backend_sends(authentication::sasl_continue("2"));
    x.frontend_sends(sasl_response("2"));
    x.backend_sends(authentication::sasl_continue("3"));
    x.frontend_sends(sasl_response("3"));
    x.backend_sends(authentication::sasl_continue("4"));
    x.frontend_sends(sasl_response("4"));
    x.backend_sends(authentication::sasl_final(""));
    x.backend_sends(error_response("something wrong"));
    assert_ok!(test_convey(x));
}

#[test]
fn auth_sasl_quadriple_error() {
    let mut x = TwoFakeStreams::new();
    x.frontend_sends(initial::startup(11, 12, hashmap!{}));
    x.backend_sends(authentication::sasl(&["a", "b"]));
    x.frontend_sends(sasl_initial_response("a"));
    x.backend_sends(authentication::sasl_continue("2"));
    x.frontend_sends(sasl_response("2"));
    x.backend_sends(authentication::sasl_continue("3"));
    x.frontend_sends(sasl_response("3"));
    x.backend_sends(authentication::sasl_continue("4"));
    x.frontend_sends(sasl_response("4"));
    x.backend_sends(error_response("something wrong"));
    assert_ok!(test_convey(x));
}
#[test]
fn auth_kerberos_unsupported() {
    let mut x = TwoFakeStreams::new();
    x.frontend_sends(initial::startup(11, 12, hashmap!{}));
    x.backend_sends(authentication::kerberos_v5);
    assert_matches!(test_convey(x), Err(Unsupported(_)));
}

#[test]
fn auth_scm_credential_unsupported() {
    let mut x = TwoFakeStreams::new();
    x.frontend_sends(initial::startup(11, 12, hashmap!{}));
    x.backend_sends(authentication::scm_credential);
    assert_matches!(test_convey(x), Err(Unsupported(_)));
}

#[test]
fn negotiate_and_error_after_auth_ok() {
    let mut x = TwoFakeStreams::new();
    x.frontend_sends(initial::startup(11, 12, hashmap!{}));
    x.backend_sends(authentication::ok);
    x.backend_sends(negotiate_protocol_version(21, &["_pq_x", "_pq_y"]));
    x.backend_sends(error_response("something goes wrong"));
    assert_ok!(test_convey(x));
}

#[test]
fn negotiate_before_and_after_auth_ok() {
    let mut x = TwoFakeStreams::new();
    x.frontend_sends(initial::startup(11, 12, hashmap!{}));
    x.backend_sends(negotiate_protocol_version(21, &["_pq_x", "_pq_y"]));
    x.backend_sends(authentication::ok);
    x.backend_sends(negotiate_protocol_version(22, &["_pq_z", "_pq_t"]));
    x.backend_sends(error_response("something goes wrong"));
    assert_ok!(test_convey(x));
}

#[test]
fn error_after_param_status() {
    let mut x = TwoFakeStreams::new();
    x.frontend_sends(initial::startup(11, 12, hashmap!{}));
    x.backend_sends(authentication::ok);
    x.backend_sends(parameter_status("param1", "value A"));
    x.backend_sends(parameter_status("param2", "value B"));
    x.backend_sends(error_response("something goes wrong"));
    assert_ok!(test_convey(x));
}

#[test]
fn error_after_backend_key() {
    let mut x = TwoFakeStreams::new();
    x.frontend_sends(initial::startup(11, 12, hashmap!{}));
    x.backend_sends(authentication::ok);
    x.backend_sends(parameter_status("param1", "value A"));
    x.backend_sends(parameter_status("param2", "value B"));
    x.backend_sends(backend_key_data(21, 22));
    x.backend_sends(error_response("something goes wrong"));
    assert_ok!(test_convey(x));
}

#[test]
fn error_after_ready() {
    let mut x = TwoFakeStreams::new();
    x.frontend_sends(initial::startup(11, 12, hashmap!{}));
    x.backend_sends(authentication::ok);
    x.backend_sends(parameter_status("param1", "value A"));
    x.backend_sends(parameter_status("param2", "value B"));
    x.backend_sends(backend_key_data(21, 22));
    x.backend_sends(ready_for_query::idle);
    x.backend_sends(error_response("something goes wrong"));
    assert_ok!(test_convey(x));
}

#[test]
fn ready_and_terminate() {
    let mut x = TwoFakeStreams::new();
    x.frontend_sends(initial::startup(11, 12, hashmap!{}));
    x.backend_sends(authentication::ok);
    x.backend_sends(parameter_status("param1", "value A"));
    x.backend_sends(parameter_status("param2", "value B"));
    x.backend_sends(backend_key_data(21, 22));
    x.backend_sends(ready_for_query::idle);
    x.frontend_sends(terminate);
    assert_ok!(test_convey(x));
}

#[test]
fn ready_and_terminate_with_notices() {
    let mut x = TwoFakeStreams::new();
    x.frontend_sends(initial::startup(11, 12, hashmap!{}));
    x.backend_sends(authentication::ok);
    x.backend_sends(notice_response("first"));
    x.backend_sends(parameter_status("param1", "value A"));
    x.backend_sends(notice_response("second"));
    x.backend_sends(parameter_status("param2", "value B"));
    x.backend_sends(notice_response("third"));
    x.backend_sends(backend_key_data(21, 22));
     x.backend_sends(notice_response("forth"));
    x.backend_sends(ready_for_query::idle);
    x.backend_sends(notice_response("fifth"));
    x.frontend_sends(terminate);
    assert_ok!(test_convey(x));
}

#[test]
fn empty_query() {
    let mut x = TwoFakeStreams::new();
    x.frontend_sends(initial::startup(11, 12, hashmap!{}));
    x.backend_sends(authentication::ok);
    x.backend_sends(backend_key_data(21, 22));
    x.backend_sends(ready_for_query::idle);
    x.frontend_sends(query(""));
    x.backend_sends(empty_query_response);
    x.backend_sends(ready_for_query::idle);
    x.frontend_sends(terminate);
    assert_ok!(test_convey(x));
}

#[test]
fn simple_select_query() {
    let mut x = TwoFakeStreams::new();
    x.frontend_sends(initial::startup(11, 12, hashmap!{}));
    x.backend_sends(authentication::ok);
    x.backend_sends(backend_key_data(21, 22));
    x.backend_sends(ready_for_query::idle);
    x.frontend_sends(query("select 2+3 as sum, null as nil"));
    x.backend_sends(notice_response("first"));
    x.backend_sends(row_description::fields(&["sum", "nil"]));
    x.backend_sends(notice_response("second"));
    x.backend_sends(data_row::columns(&[Some("5"), None]));
    x.backend_sends(notice_response("third"));
    x.backend_sends(command_complete("SELECT 1"));
    x.backend_sends(notice_response("forth"));
    x.backend_sends(ready_for_query::idle);
    x.backend_sends(notice_response("fifth"));
    x.frontend_sends(terminate);
    assert_ok!(test_convey(x));
}

#[test]
fn simple_select_query_with_notices() {
    let mut x = TwoFakeStreams::new();
    x.frontend_sends(initial::startup(11, 12, hashmap!{}));
    x.backend_sends(authentication::ok);
    x.backend_sends(backend_key_data(21, 22));
    x.backend_sends(ready_for_query::idle);
    x.frontend_sends(query("select 2+3 as sum, null as nil"));
    x.backend_sends(row_description::fields(&["sum", "nil"]));
    x.backend_sends(data_row::columns(&[Some("5"), None]));
    x.backend_sends(command_complete("SELECT 1"));
    x.backend_sends(ready_for_query::idle);
    x.frontend_sends(terminate);
    assert_ok!(test_convey(x));
}

#[test]
fn single_changing_query() {
    let mut x = TwoFakeStreams::new();
    x.frontend_sends(initial::startup(11, 12, hashmap!{}));
    x.backend_sends(authentication::ok);
    x.backend_sends(backend_key_data(21, 22));
    x.backend_sends(ready_for_query::idle);
    x.frontend_sends(query("insert values('new') into t1"));
    x.backend_sends(command_complete("INSERT 1"));
    x.backend_sends(ready_for_query::idle);
    x.frontend_sends(terminate);
    assert_ok!(test_convey(x));
}

#[test]
fn multiple_statements_in_query() {
    let mut x = TwoFakeStreams::new();
    x.frontend_sends(initial::startup(11, 12, hashmap!{}));
    x.backend_sends(authentication::ok);
    x.backend_sends(backend_key_data(21, 22));
    x.backend_sends(ready_for_query::idle);
    x.frontend_sends(query("select 2+3 as sum; update t1 set key=0; delete from t1; select null as nil"));
    x.backend_sends(row_description::fields(&["sum"]));
    x.backend_sends(data_row::columns(&[Some("5"), None]));
    x.backend_sends(command_complete("SELECT 1"));
    x.backend_sends(command_complete("UPDATE 9000"));
    x.backend_sends(command_complete("DELETE 9000"));
    x.backend_sends(row_description::fields(&["nil"]));
    x.backend_sends(data_row::columns(&[Some("null"), None]));
    x.backend_sends(command_complete("SELECT 1"));
    x.backend_sends(ready_for_query::idle);
    x.frontend_sends(terminate);
    assert_ok!(test_convey(x));
}

#[test]
fn multiple_statements_in_query_with_error_between() {
    let mut x = TwoFakeStreams::new();
    x.frontend_sends(initial::startup(11, 12, hashmap!{}));
    x.backend_sends(authentication::ok);
    x.backend_sends(backend_key_data(21, 22));
    x.backend_sends(ready_for_query::idle);
    x.frontend_sends(query("select 2+3 as sum; update t1 set key=0; delete from t1; select null as nil"));
    x.backend_sends(row_description::fields(&["sum"]));
    x.backend_sends(data_row::columns(&[Some("5"), None]));
    x.backend_sends(command_complete("SELECT 1"));
    x.backend_sends(error_response("relation \"t1\" does not exist"));
    x.backend_sends(ready_for_query::idle);
    x.frontend_sends(terminate);
    assert_ok!(test_convey(x));
}

#[test]
fn multiple_queries_with_error_between() {
    let mut x = TwoFakeStreams::new();
    x.frontend_sends(initial::startup(11, 12, hashmap!{}));
    x.backend_sends(authentication::ok);
    x.backend_sends(backend_key_data(21, 22));
    x.backend_sends(ready_for_query::idle);
    x.frontend_sends(query("update t1 set key=0"));
    x.backend_sends(error_response("relation \"t1\" does not exist"));
    x.backend_sends(ready_for_query::idle);
    x.frontend_sends(query("select 2+3 as sum"));
    x.backend_sends(row_description::fields(&["sum"]));
    x.backend_sends(data_row::columns(&[Some("5")]));
    x.backend_sends(command_complete("SELECT 1"));
    x.backend_sends(ready_for_query::idle);
    x.frontend_sends(terminate);
    assert_ok!(test_convey(x));
}

#[test]
fn ext_query_parse_error() {
    let mut x = TwoFakeStreams::new();
    x.frontend_sends(initial::startup(11, 12, hashmap!{}));
    x.backend_sends(authentication::ok);
    x.backend_sends(backend_key_data(21, 22));
    x.backend_sends(ready_for_query::idle);
    x.frontend_sends(parse(()));
    x.backend_sends(error_response(""));
    x.backend_sends(ready_for_query::idle);
    x.frontend_sends(terminate);
    assert_ok!(test_convey(x));
}

#[test]
fn ext_query_bind_error() {
    let mut x = TwoFakeStreams::new();
    x.frontend_sends(initial::startup(11, 12, hashmap!{}));
    x.backend_sends(authentication::ok);
    x.backend_sends(backend_key_data(21, 22));
    x.backend_sends(ready_for_query::idle);
    x.frontend_sends(parse(()));
    x.backend_sends(parse_complete);
    x.frontend_sends(bind(()));
    x.backend_sends(error_response(""));
    x.backend_sends(ready_for_query::idle);
    x.frontend_sends(terminate);
    assert_ok!(test_convey(x));
}

#[test]
fn ext_query_execute_error() {
    let mut x = TwoFakeStreams::new();
    x.frontend_sends(initial::startup(11, 12, hashmap!{}));
    x.backend_sends(authentication::ok);
    x.backend_sends(backend_key_data(21, 22));
    x.backend_sends(ready_for_query::idle);
    x.frontend_sends(parse(()));
    x.backend_sends(parse_complete);
    x.frontend_sends(bind(()));
    x.backend_sends(bind_complete);
    x.frontend_sends(execute(()));
    x.backend_sends(error_response(""));
    x.frontend_sends(sync);
    x.backend_sends(ready_for_query::idle);
    x.frontend_sends(terminate);
    assert_ok!(test_convey(x));
}

#[test]
fn ext_query_execute_empty() {
    let mut x = TwoFakeStreams::new();
    x.frontend_sends(initial::startup(11, 12, hashmap!{}));
    x.backend_sends(authentication::ok);
    x.backend_sends(backend_key_data(21, 22));
    x.backend_sends(ready_for_query::idle);
    x.frontend_sends(parse(()));
    x.backend_sends(parse_complete);
    x.frontend_sends(bind(()));
    x.backend_sends(bind_complete);
    x.frontend_sends(execute(()));
    x.backend_sends(empty_query_response);
    x.frontend_sends(sync);
    x.backend_sends(ready_for_query::idle);
    x.frontend_sends(terminate);
    assert_ok!(test_convey(x));
}

#[test]
fn ext_query_execute_no_rows() {
    let mut x = TwoFakeStreams::new();
    x.frontend_sends(initial::startup(11, 12, hashmap!{}));
    x.backend_sends(authentication::ok);
    x.backend_sends(backend_key_data(21, 22));
    x.backend_sends(ready_for_query::idle);
    x.frontend_sends(parse(()));
    x.backend_sends(parse_complete);
    x.frontend_sends(bind(()));
    x.backend_sends(bind_complete);
    x.frontend_sends(execute(()));
    x.backend_sends(command_complete(""));
    x.frontend_sends(sync);
    x.backend_sends(ready_for_query::idle);
    x.frontend_sends(terminate);
    assert_ok!(test_convey(x));
}

#[test]
fn ext_query_execute_few_rows() {
    let mut x = TwoFakeStreams::new();
    x.frontend_sends(initial::startup(11, 12, hashmap!{}));
    x.backend_sends(authentication::ok);
    x.backend_sends(backend_key_data(21, 22));
    x.backend_sends(ready_for_query::idle);
    x.frontend_sends(parse(()));
    x.backend_sends(parse_complete);
    x.frontend_sends(bind(()));
    x.backend_sends(bind_complete);
    x.frontend_sends(execute(()));
    x.backend_sends(data_row::columns(&[]));
    x.backend_sends(data_row::columns(&[]));
    x.backend_sends(data_row::columns(&[]));
    x.backend_sends(command_complete(""));
    x.frontend_sends(sync);
    x.backend_sends(ready_for_query::idle);
    x.frontend_sends(terminate);
    assert_ok!(test_convey(x));
}

#[test]
fn ext_query_execute_many_rows() {
    let mut x = TwoFakeStreams::new();
    x.frontend_sends(initial::startup(11, 12, hashmap!{}));
    x.backend_sends(authentication::ok);
    x.backend_sends(backend_key_data(21, 22));
    x.backend_sends(ready_for_query::idle);
    x.frontend_sends(parse(()));
    x.backend_sends(parse_complete);
    x.frontend_sends(bind(()));
    x.backend_sends(bind_complete);
    x.frontend_sends(execute(()));
    x.backend_sends(data_row::columns(&[]));
    x.backend_sends(data_row::columns(&[]));
    x.backend_sends(data_row::columns(&[]));
    x.backend_sends(portal_suspended);
    x.frontend_sends(execute(()));
    x.backend_sends(data_row::columns(&[]));
    x.backend_sends(data_row::columns(&[]));
    x.backend_sends(command_complete(""));
    x.frontend_sends(sync);
    x.backend_sends(ready_for_query::idle);
    x.frontend_sends(terminate);
    assert_ok!(test_convey(x));
}

fn test_convey(fake_streams: TwoFakeStreams) -> ConveyResult<()> {
    let split = fake_streams.split_backend_frontend();
    let mut expected_conveyed = split.filter_messages();
    let mut conveyor = Conveyor::new(
        split.frontend(),
        split.backend(),
        FakeTlsServer(),
        FakeTlsClient(),
        |side, msg| {
            let (exp_side, exp_msg) = assert_some!(expected_conveyed.next());
            assert_eq!(exp_side, side);
            assert_messages_eq(exp_msg, msg);
        },
    );
    let convey_result = task::block_on(conveyor.go());
    assert_none!(expected_conveyed.next(),
        "expected but not conveyed {:?}", expected_conveyed.collect::<Vec<_>>());
    let unread = split.untaken().collect::<Vec<_>>();
    assert!(unread.is_empty(), "untaken messages {:?}", unread);
    convey_result
}

fn assert_messages_eq(x: &dyn Message, y: &dyn Message) -> bool {
    use crate::msg::body::*;
    assert_eq_if_same_type::<Authentication>(x, y) ||
        assert_eq_if_same_type::<BackendKeyData>(x, y) ||
        assert_eq_if_same_type::<BindComplete>(x, y) ||
        assert_eq_if_same_type::<CommandComplete>(x, y) ||
        assert_eq_if_same_type::<DataRow>(x, y) ||
        assert_eq_if_same_type::<EmptyQueryResponse>(x, y) ||
        assert_eq_if_same_type::<ErrorResponse>(x, y) ||
        assert_eq_if_same_type::<NegotiateProtocolVersion>(x, y) ||
        assert_eq_if_same_type::<NoticeResponse>(x, y) ||
        assert_eq_if_same_type::<ParameterStatus>(x, y) ||
        assert_eq_if_same_type::<ParseComplete>(x, y) ||
        assert_eq_if_same_type::<PortalSuspended>(x, y) ||
        assert_eq_if_same_type::<ReadyForQuery>(x, y) ||
        assert_eq_if_same_type::<RowDescription>(x, y) ||
        assert_eq_if_same_type::<Bind>(x, y) ||
        assert_eq_if_same_type::<Execute>(x, y) ||
        assert_eq_if_same_type::<GssResponse>(x, y) ||
        assert_eq_if_same_type::<Initial>(x, y) ||
        assert_eq_if_same_type::<Parse>(x, y) ||
        assert_eq_if_same_type::<Password>(x, y) ||
        assert_eq_if_same_type::<Query>(x, y) ||
        assert_eq_if_same_type::<SaslInitialResponse>(x, y) ||
        assert_eq_if_same_type::<SaslResponse>(x, y) ||
        assert_eq_if_same_type::<Sync>(x, y) ||
        assert_eq_if_same_type::<Terminate>(x, y) ||
        panic!("test suite internal problem: some Message trait implementation is missing")
}

fn assert_eq_if_same_type<M>(x: &dyn Message, y: &dyn Message) -> bool
    where M: 'static + Debug + PartialEq {
    if let Some(x) = downcast_msg::<M>(x) {
        if let Some(y) = downcast_msg::<M>(y) {
            assert_eq!(x, y);
            return true
        }
    }
    false
}
