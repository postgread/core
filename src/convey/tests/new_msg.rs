#![allow(non_upper_case_globals)]

use crate::msg::body;

pub mod authentication {
    use crate::msg::body::authentication::*;

    pub const ok: Authentication = Authentication::Ok;

    pub const cleartext_password: Authentication = Authentication::CleartextPassword;

    pub const gss: Authentication = Authentication::Gss;
    pub fn gss_continue(auth_data: &[u8]) -> Authentication {
        Authentication::GssContinue { auth_data: auth_data.into() }
    }

    pub const kerberos_v5: Authentication = Authentication::KerberosV5;

    pub fn md5_password(salt: &[u8; 4]) -> Authentication {
        Authentication::Md5Password { salt: *salt }
    }

    pub fn sasl(auth_mechanisms: &[&'static str]) -> Authentication {
        let auth_mechanisms = auth_mechanisms.iter().map(|s| (*s).into()).collect();
        Authentication::Sasl { auth_mechanisms }
    }

    pub fn sasl_continue(challenge_data: &'static str) -> Authentication {
        Authentication::SaslContinue { challenge_data: challenge_data.into() }
    }

    pub fn sasl_final(additional_data: &'static str) -> Authentication {
        Authentication::SaslFinal { additional_data: additional_data.into() }
    }

    pub const scm_credential: Authentication = Authentication::ScmCredential;

    pub const sspi: Authentication = Authentication::Sspi;
}

pub fn backend_key_data(process_id: u32, secret_key: u32) -> body::BackendKeyData {
    body::BackendKeyData {
        process_id,
        secret_key
    }
}

pub fn bind(_: ()) -> body::Bind {
    body::Bind {
        prepared_statement_name: "".into(),
        portal_name: "".into(),
        parameters_formats: vec![],
        parameters_values: vec![],
        results_formats: vec![],
    }
}

pub const bind_complete: body::BindComplete = body::BindComplete();

pub fn command_complete(tag: &'static str) -> body::CommandComplete {
    body::CommandComplete {
            tag: tag.into(),
        }
}

pub mod data_row {
    use crate::msg::body::data_row::*;
    use crate::msg::parts::{Bytes, Value};

    pub fn columns(values: &[Option<&'static str>]) -> DataRow {
        let columns = values.iter().map(|opt| {
            opt.map_or(Value::Null, |str| Value::Bytes(Bytes(str.into())))
        }).collect();
        DataRow { columns }
    }
}

pub const empty_query_response: body::EmptyQueryResponse = body::EmptyQueryResponse {};


pub fn error_response(message: &'static str) -> body::ErrorResponse {
    body::ErrorResponse(body::error_and_notice_responses::ErrorOrNoticeFields {
        severity: Some("ERROR".into()),
        message: Some(message.into()),
        ..Default::default()
    })
}

pub fn execute(_: ()) -> body::Execute {
    body::Execute {
        portal_name: "".into(),
        rows_limit: 0,
    }
}

pub fn gss_response(response: &[u8]) -> body::GssResponse {
    body::GssResponse(response.into())
}

pub fn notice_response(message: &'static str) -> body::NoticeResponse {
    body::NoticeResponse(body::error_and_notice_responses::ErrorOrNoticeFields {
        severity: Some("NOTICE".into()),
        message: Some(message.into()),
        ..Default::default()
    })
}

pub mod initial {
    use crate::msg::body::initial::*;
    use ::std::collections::HashMap;

    pub fn startup(
        major: u16,
        minor: u16,
        params: HashMap<&'static str, &'static str>
    ) -> Initial {
        let to_startup_param = |(name, value): (&&'static str, &&'static str)| {
            StartupParam {
                name: (*name).into(),
                value: (*value).into(),
            }
        };
        Initial::Startup(Startup {
            version: Version { major, minor },
            params: params.iter().map(to_startup_param).collect(),
        })
    }

    pub fn cancel(process_id: u32, secret_key: u32) -> Initial {
        Initial::Cancel(Cancel {
            process_id,
            secret_key,
        })
    }

    pub const tls: Initial = Initial::TLS;
}

pub fn negotiate_protocol_version(newest_backend_minor: u32, unrecognized_options: &[&'static str]) -> body::NegotiateProtocolVersion {
    let unrecognized_options = unrecognized_options.iter().map(|str| (*str).into()).collect();
    body::NegotiateProtocolVersion {
        newest_backend_minor,
        unrecognized_options,
    }
}

pub fn parameter_status(name: &'static str, value: &'static str) -> body::ParameterStatus {
    body::ParameterStatus {
        name: name.into(),
        value: value.into(),
    }
}

pub fn parse(_: ()) -> body::Parse {
    body::Parse {
        prepared_statement_name: "".into(),
        query: "".into(),
        parameters_types: vec![],
    }
}

pub const parse_complete: body::ParseComplete = body::ParseComplete();

pub fn password(password: &'static str) -> body::Password {
    body::Password(password.into())
}

pub const portal_suspended: body::PortalSuspended = body::PortalSuspended();

pub fn query(sql: &'static str) -> body::Query {
   body::Query(sql.into())
}

pub mod ready_for_query {
    use crate::msg::body::ready_for_query::*;

    pub const idle: ReadyForQuery = ReadyForQuery { status: Status::Idle };
}

pub mod row_description {
    use crate::msg::parts::Format;
    use crate::msg::body::row_description::*;

    pub fn fields(names: &[&'static str]) -> RowDescription {
        let fields = names.iter().enumerate().map(|(i, name)| Field {
            name: (*name).into(),
            column_attr_num: i as u16,
            type_size: 10 * i as i16,
            column_oid: 100 * i as u32,
            type_oid: 1000 * i as u32,
            type_modifier: 10000 * i as i32,
            format: Format::Text,
        }).collect();
        RowDescription { fields }
    }
}

pub fn sasl_initial_response(selected_mechanism: &'static str) -> body::SaslInitialResponse {
    body::SaslInitialResponse {
        selected_mechanism: selected_mechanism.into(),
        mechanism_data: None,
    }
}

pub fn sasl_response(mechanism_data: &'static str) -> body::SaslResponse {
    body::SaslResponse {
        mechanism_data: mechanism_data.into(),
    }
}

pub const sync: body::Sync = body::Sync();

pub const terminate: body::Terminate = body::Terminate {};
