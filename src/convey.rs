#[cfg(test)] mod tests;
mod reader;
mod tlsable;
mod writer;

use reader::*;
use tlsable::{Tlsable, TlsResult};
use writer::*;

use crate::msg::body::*;
use crate::msg::type_byte::TypeByte;
use crate::msg::util::decode::{MsgDecode, Problem as DecodeProblem};
use crate::msg::util::encode::{Problem as EncodeProblem};
use crate::msg::util::read::*;
use crate::tls::interface::{TlsClient, TlsServer};

use ::core::any::Any;
use ::core::fmt::Debug;
use ::core::marker;
use ::futures::future::{self, Either, FutureExt};
use ::futures::io::{AsyncRead, AsyncWrite};
use ::std::convert::TryFrom;
use ::std::io::{Error as IoError, Result as IoResult};

#[derive(Debug)]
pub enum Error {
    DecodeError(DecodeProblem),
    EncodeError(EncodeProblem),
    IoError(IoError),
    TlsError(tlsable::TlsError),
    TlsRequestedInsideTls,
    LeftUndecoded(usize),
    Todo(String),
    UnexpectedType(State, Side, TypeByte),
    UnknownType(Side, u8),
    Unsupported(&'static str),
}

pub type ConveyResult<T> = Result<T, Error>;

// PostgreSQL protocol documentation uses terms "frontend" for client and "backend" for server.
#[derive(Clone, Copy, Debug, PartialEq)]
pub enum Side {
    Backend,
    Frontend,
}

#[derive(Clone, Copy, Debug)]
pub enum State {
    // From backend point of view ("AskedX" means "backend asked X", "GotX" means "backend got X from frontend").
    AbortedExtendedQuery,
    AbortedParsingOrBinding,
    AbortedSimpleQuery,
    AnsweringToExtendedQuery,
    AnsweringToSimpleQuery,
    AskedCleartextPassword,
    AskedGssResponse,
    AskedMd5Password,
    AskedSaslInitialResponse,
    AskedSaslResponse,
    Authenticated,
    Bound,
    CompletedExtendedQuery,
    CompletedSimpleCommand,
    ExecutingExtendedQuery,
    FinishedSasl,
    GotAnySaslResponse,
    GotBinding,
    GotCleartextPassword,
    GotGssResponse,
    GotMd5Password,
    GotPreparedStatement,
    GotSimpleQuery,
    GotStartup,
    GotSync,
    ReadyForQuery,
    SeenEmptyExtendedQuery,
    SeenEmptySimpleQuery,
    SentAllBackendParams,
    SuspendedExtendedQuery,
}

pub trait Message : Debug {
    fn as_any(&self) -> &dyn Any;

    fn clone_boxed(&self) -> Box<dyn Message + Send + marker::Sync>;
}

impl<M: 'static + Debug + MsgDecode + Send + marker::Sync> Message for M {
    fn as_any(&self) -> &dyn Any {
        self
    }

    fn clone_boxed(&self) -> Box<dyn Message + Send + marker::Sync> {
        Box::new(self.clone())
    }
}

pub fn downcast_msg<M: 'static>(msg: &dyn Message) -> Option<&M> {
    msg.as_any().downcast_ref::<M>()
}

pub async fn convey<FrontPlain, BackPlain, FrontTlsServer, BackTlsClient, Callback>(
    frontend: FrontPlain,
    backend: BackPlain,
    frontend_tls_server: FrontTlsServer,
    backend_tls_client: BackTlsClient,
    callback: Callback,
) -> ConveyResult<()>
where
    FrontPlain: AsyncRead + AsyncWrite + Send + Unpin,
    BackPlain: AsyncRead + AsyncWrite + Send + Unpin,
    FrontTlsServer: TlsServer<FrontPlain> + Send,
    BackTlsClient: TlsClient<BackPlain> + Send,
    FrontTlsServer::Tls: AsyncRead + AsyncWrite,
    BackTlsClient::Tls: AsyncRead + AsyncWrite,
    Callback: Fn(Side, &(dyn Message + Send + marker::Sync + 'static)) + Send,
{
    Conveyor::new(
        frontend,
        backend,
        frontend_tls_server,
        backend_tls_client,
        callback,
    ).go().await
}

struct Conveyor<FrontPlain, BackPlain, FrontTlsServer, BackTlsClient, Callback>
where
    FrontPlain: Send + Unpin,
    BackPlain: Send + Unpin,
    FrontTlsServer: TlsServer<FrontPlain>,
    BackTlsClient: TlsClient<BackPlain>,
{
    frontend: Tlsable<FrontPlain, FrontTlsServer::Tls>,
    backend: Tlsable<BackPlain, BackTlsClient::Tls>,
    frontend_tls_server: FrontTlsServer,
    backend_tls_client: BackTlsClient,
    callback: Callback,
}

#[derive(PartialEq)]
#[must_use]
enum Processing {
    Continue,
    Finished,
}

use Error::*;
use Processing::*;
use Side::*;

// https://www.postgresql.org/docs/12/protocol-overview.html#PROTOCOL-MESSAGE-CONCEPTS
// > 52.1.1. Messaging Overview
// > ...
// > To avoid losing synchronization with the message stream, both servers and clients typically
// > read an entire message into a buffer (using the byte count) before attempting to process its
// > contents. This allows easy recovery if an error is detected while processing the contents.
// > In extreme situations (such as not having enough memory to buffer the message), the receiver
// > can use the byte count to determine how much input to skip before it resumes reading messages.
// > Conversely, both servers and clients must take care never to send an incomplete message. This
// > is commonly done by marshaling the entire message in a buffer before beginning to send it. If a
// > communications failure occurs partway through sending or receiving a message, the only sensible
// > response is to abandon the connection, since there is little hope of recovering
// > message-boundary synchronization.

impl<'a, FrontPlain, BackPlain, FrontTlsServer, BackTlsClient, Callback>
Conveyor<FrontPlain, BackPlain, FrontTlsServer, BackTlsClient, Callback>
where
    FrontPlain: Reader + Writer,
    BackPlain: Reader + Writer,
    FrontTlsServer: TlsServer<FrontPlain> + Send,
    BackTlsClient: TlsClient<BackPlain> + Send,
    FrontTlsServer::Tls: Reader + Writer,
    BackTlsClient::Tls: Reader + Writer,
    Callback: FnMut(Side, &(dyn Message + Send + marker::Sync + 'static)) + Send,
{
    fn new(
        frontend: FrontPlain,
        backend: BackPlain,
        frontend_tls_server: FrontTlsServer,
        backend_tls_client: BackTlsClient,
        callback: Callback,
    ) -> Self {
        Conveyor {
            frontend: Tlsable::Plain(frontend),
            backend: Tlsable::Plain(backend),
            frontend_tls_server,
            backend_tls_client,
            callback,
        }
    }

    #[allow(clippy::cognitive_complexity)]
    async fn go(&mut self) -> ConveyResult<()> {
        // https://www.postgresql.org/docs/12/protocol-overview.html
        // > 52.1.1. Messaging Overview
        // > All communication is through a stream of messages. The first byte of a message
        // > identifies the message type, and the next four bytes specify the length <...>. The
        // > remaining contents of the message are determined by the message type. <...> The very
        // first message sent by the client (the startup message) has no initial message-type byte.

        // Every "frontend" in the protocol documentation should be considered as referring to:
        //  * an actual frontend connected to Postgread,
        //  * Postgread itself being a frontend connected to an actual backend.
        // Likewise, every "backend" in the documentation should be considered as referring to:
        //  * an actual backend accepted a connection from Postgread,
        //  * Postgread itself being a backend accepted a connection from an actual frontend.
        // When Postgread doesn't proxy messages "as is", but changes the flow, it becomes very
        // important to distinguish *which* "frontend"/"backend" from the documentation is meant.

        if self.process_initial().await? == Finished {
            return Ok(())
        }
        let mut state = State::GotStartup;
        loop {
            let (side, type_byte) = self.read_type_byte_from_both().await?;
            use TypeByte as T;
            if cfg!(test) {
                eprintln!("conveyor got {:?} from {:?} on state={:?}", type_byte, side, state);
            }
            state = match (side, type_byte, state) {
                (Backend, T::Authentication, _) => {
                    self.process_backend_authentication(type_byte, state).await
                },
                (Backend, T::BackendKeyData, State::Authenticated) => {
                    self.read_backend_through::<BackendKeyData>().await?;
                    Ok(State::SentAllBackendParams)
                },
                (Frontend, T::Bind, State::ReadyForQuery) => {
                    self.read_frontend_through::<Bind>().await?;
                    Ok(State::GotBinding)
                }
                (Backend, T::BindComplete, State::GotBinding) => {
                    self.read_backend_through::<BindComplete>().await?;
                    Ok(State::ReadyForQuery)
                }
                (Backend, T::CommandComplete, State::AnsweringToSimpleQuery) |
                (Backend, T::CommandComplete, State::CompletedSimpleCommand) |
                (Backend, T::CommandComplete, State::GotSimpleQuery) => {
                    self.read_backend_through::<CommandComplete>().await?;
                    Ok(State::CompletedSimpleCommand)
                },
                (Backend, T::CommandComplete, State::AnsweringToExtendedQuery) |
                (Backend, T::CommandComplete, State::ExecutingExtendedQuery) => {
                    self.read_backend_through::<CommandComplete>().await?;
                    Ok(State::CompletedExtendedQuery)
                },
                (Backend, T::DataRow, State::AnsweringToSimpleQuery) => {
                    self.read_backend_through::<DataRow>().await?;
                    Ok(State::AnsweringToSimpleQuery)
                },
                (Backend, T::DataRow, State::AnsweringToExtendedQuery) |
                (Backend, T::DataRow, State::ExecutingExtendedQuery) => {
                    self.read_backend_through::<DataRow>().await?;
                    Ok(State::AnsweringToExtendedQuery)
                },
                (Backend, T::EmptyQueryResponse, State::ExecutingExtendedQuery) => {
                    self.read_backend_through::<EmptyQueryResponse>().await?;
                    Ok(State::SeenEmptyExtendedQuery)
                },
                (Backend, T::EmptyQueryResponse, State::GotSimpleQuery) => {
                    self.read_backend_through::<EmptyQueryResponse>().await?;
                    Ok(State::SeenEmptySimpleQuery)
                },
                (Backend, T::Execute_or_ErrorResponse, State::AbortedSimpleQuery) |
                (Backend, T::Execute_or_ErrorResponse, State::AnsweringToSimpleQuery) |
                (Backend, T::Execute_or_ErrorResponse, State::CompletedSimpleCommand) |
                (Backend, T::Execute_or_ErrorResponse, State::GotSimpleQuery) |
                (Backend, T::Execute_or_ErrorResponse, State::SeenEmptySimpleQuery) => {
                    self.read_backend_through::<ErrorResponse>().await?;
                    Ok(State::AbortedSimpleQuery)
                },
                (Backend, T::Execute_or_ErrorResponse, State::GotBinding) |
                (Backend, T::Execute_or_ErrorResponse, State::GotPreparedStatement) => {
                    self.read_backend_through::<ErrorResponse>().await?;
                    Ok(State::AbortedParsingOrBinding)
                },
                (Backend, T::Execute_or_ErrorResponse, State::AbortedExtendedQuery) |
                (Backend, T::Execute_or_ErrorResponse, State::AnsweringToExtendedQuery) |
                (Backend, T::Execute_or_ErrorResponse, State::CompletedExtendedQuery) |
                (Backend, T::Execute_or_ErrorResponse, State::ExecutingExtendedQuery) |
                (Backend, T::Execute_or_ErrorResponse, State::SeenEmptyExtendedQuery) => {
                    self.read_backend_through::<ErrorResponse>().await?;
                    Ok(State::AbortedExtendedQuery)
                },
                (Backend, T::Execute_or_ErrorResponse, _) => {
                    self.read_backend_through::<ErrorResponse>().await?;
                    return Ok(())
                },
                (Frontend, T::Execute_or_ErrorResponse, State::ReadyForQuery) |
                (Frontend, T::Execute_or_ErrorResponse, State::SuspendedExtendedQuery) => {
                    self.read_frontend_through::<Execute>().await?;
                    Ok(State::ExecutingExtendedQuery)
                },
                (Frontend, T::GssResponse_or_Password_or_SaslResponses, State::AskedCleartextPassword) => {
                    self.read_frontend_through::<Password>().await?;
                    Ok(State::GotCleartextPassword)
                },
                (Frontend, T::GssResponse_or_Password_or_SaslResponses, State::AskedGssResponse) => {
                    self.read_frontend_through::<GssResponse>().await?;
                    Ok(State::GotGssResponse)
                },
                (Frontend, T::GssResponse_or_Password_or_SaslResponses, State::AskedMd5Password) => {
                    self.read_frontend_through::<Password>().await?;
                    Ok(State::GotMd5Password)
                },
                (Frontend, T::GssResponse_or_Password_or_SaslResponses, State::AskedSaslInitialResponse) => {
                    self.read_frontend_through::<SaslInitialResponse>().await?;
                    Ok(State::GotAnySaslResponse)
                },
                (Frontend, T::GssResponse_or_Password_or_SaslResponses, State::AskedSaslResponse) => {
                    self.read_frontend_through::<SaslResponse>().await?;
                    Ok(State::GotAnySaslResponse)
                },
                (Backend, T::NegotiateProtocolVersion, State::GotStartup) |
                (Backend, T::NegotiateProtocolVersion, State::Authenticated) => {
                    self.read_backend_through::<NegotiateProtocolVersion>().await?;
                    Ok(state)
                },
                (Backend, T::NoticeResponse, _) => {
                    self.read_backend_through::<NoticeResponse>().await?;
                    Ok(state)
                },
                (Backend, T::ParameterStatus_or_Sync, State::Authenticated) => {
                    self.read_backend_through::<ParameterStatus>().await?;
                    Ok(State::Authenticated)
                },
                (Frontend, T::ParameterStatus_or_Sync, State::AbortedExtendedQuery) |
                (Frontend, T::ParameterStatus_or_Sync, State::CompletedExtendedQuery) |
                (Frontend, T::ParameterStatus_or_Sync, State::SeenEmptyExtendedQuery) |
                (Frontend, T::ParameterStatus_or_Sync, State::SuspendedExtendedQuery) => {
                    self.read_frontend_through::<Sync>().await?;
                    Ok(State::GotSync)
                },
                (Frontend, T::Parse, State::ReadyForQuery) => {
                    self.read_frontend_through::<Parse>().await?;
                    Ok(State::GotPreparedStatement)
                },
                (Backend, T::ParseComplete, State::GotPreparedStatement) => {
                    self.read_backend_through::<ParseComplete>().await?;
                    Ok(State::ReadyForQuery)
                },
                (Backend, T::PortalSuspended, State::AnsweringToExtendedQuery) => {
                    self.read_backend_through::<PortalSuspended>().await?;
                    Ok(State::SuspendedExtendedQuery)
                },
                (Frontend, T::Query, State::ReadyForQuery) => {
                    self.read_frontend_through::<Query>().await?;
                    Ok(State::GotSimpleQuery)
                },
                (Backend, T::ReadyForQuery, State::AbortedParsingOrBinding) |
                (Backend, T::ReadyForQuery, State::AbortedSimpleQuery) |
                (Backend, T::ReadyForQuery, State::CompletedSimpleCommand) |
                (Backend, T::ReadyForQuery, State::SentAllBackendParams) |
                (Backend, T::ReadyForQuery, State::GotSync) |
                (Backend, T::ReadyForQuery, State::SeenEmptySimpleQuery) => {
                    self.read_backend_through::<ReadyForQuery>().await?;
                    Ok(State::ReadyForQuery)
                },
                (Backend, T::RowDescription, State::GotSimpleQuery) |
                (Backend, T::RowDescription, State::CompletedSimpleCommand) => {
                    self.read_backend_through::<RowDescription>().await?;
                    Ok(State::AnsweringToSimpleQuery)
                },
                (Frontend, T::Terminate, State::ReadyForQuery) => {
                    self.read_frontend_through::<Terminate>().await?;
                    return Ok(())
                },
                _ => Err(UnexpectedType(state, side, type_byte)),
            }?
        }
    }

    async fn process_initial(&mut self) -> ConveyResult<Processing> {
        match self.read_frontend_through::<Initial>().await? {
            Initial::Startup(_) => Ok(Continue),
            Initial::Cancel(_) => Ok(Finished),
            // https://www.postgresql.org/docs/12/protocol-flow.html#id-1.10.5.7.12
            // > 52.2.10. SSL Session Encryption
            // > ...
            // > To initiate an SSL-encrypted connection, the frontend initially sends an SSLRequest
            // > message rather than a StartupMessage. The server then responds with a single byte
            // > containing S or N, indicating that it is willing or unwilling to perform SSL,
            // > respectively. The frontend might close the connection at this point if it is
            // > dissatisfied with the response.
            Initial::TLS => {
                // So the frontend sent an SSLRequest, and Postgread sent it to the backend which
                // responded with a single byte (S, N, or E).
                let tls_response = read_type_byte(&mut self.backend).await?;
                match tls_response {
                    TLS_NOT_SUPPORTED => {
                        // https://www.postgresql.org/docs/12/protocol-flow.html#id-1.10.5.7.12
                        // > 52.2.10. SSL Session Encryption
                        // > ...
                        // > To continue after N, send the usual StartupMessage and proceed without
                        // > encryption.
                        //
                        // Postgread silently agrees to communicate with the backend without
                        // encryption. However, it will support encryption for the frontend. And the
                        // frontend will send a StartupMessage which will be re-sent to the backend.
                    },
                    TLS_SUPPORTED => {
                        // https://www.postgresql.org/docs/12/protocol-flow.html#id-1.10.5.7.12
                        // > 52.2.10. SSL Session Encryption
                        // > ...
                        // > To continue after S, perform an SSL startup handshake <...> with the
                        // > server. If this is successful, continue with sending the usual
                        // > StartupMessage. In this case the StartupMessage and all subsequent data
                        // > will be SSL-encrypted.
                        self.switch_backend_to_tls().await?;
                        // The frontend will send a StartupMessage which will be re-sent to the
                        // backend by Postgread.
                    },
                    ErrorResponse::TYPE_BYTE => {
                        // https://www.postgresql.org/docs/12/protocol-flow.html#id-1.10.5.7.12
                        // > 52.2.10. SSL Session Encryption
                        // > ...
                        // > The frontend should also be prepared to handle an ErrorMessage response
                        // > to SSLRequest from the server. This would only occur if the server
                        // > predates the addition of SSL support to PostgreSQL. (Such servers are
                        // > now very ancient, and likely do not exist in the wild anymore.) In this
                        // > case the connection must be closed <...>
                        //
                        self.read_backend_through::<ErrorResponse>().await?;
                        // Postgread got an error from the backend and re-sent it to the frontend.
                        return Ok(Finished)
                    }
                    _ => {
                        return Err(UnknownType(Backend, tls_response))
                    },
                }
                // Whether the backend support TLS or not, Postgread does:
                write(&mut self.frontend, &[TLS_SUPPORTED]).await?;
                // https://www.postgresql.org/docs/12/protocol-flow.html#id-1.10.5.7.12
                // > 52.2.10. SSL Session Encryption
                // > ...
                // > To continue after S, perform an SSL startup handshake <...> with the server.
                // > If this is successful, continue with sending the usual StartupMessage. In
                // > this case the StartupMessage and all subsequent data will be SSL-encrypted.
                //
                // This time the cited instruction refers to the frontend (not Postgread). What
                // Postgread should do is accept a TLS/SSL handshake.
                self.switch_frontend_to_tls().await?;
                match self.read_frontend_through::<Initial>().await? {
                    // The frontend sent a StartupMessage via TLS to Postgread which in turn re-sent
                    // this message to the backend (via TLS or not, depending on the previous
                    // backend's response).
                    Initial::Startup(_) => Ok(Continue),
                    Initial::Cancel(_) => Ok(Finished),
                    Initial::TLS => Err(TlsRequestedInsideTls),
                }
            }
        }
    }

    async fn process_backend_authentication(&mut self, type_byte: TypeByte, state: State) -> ConveyResult<State> {
        use Authentication as Auth;
        let authentication = self.read_backend_through::<Authentication>().await?;
        match (authentication, &state) {
            (Auth::CleartextPassword, State::GotStartup) =>
                Ok(State::AskedCleartextPassword),
            (Auth::Gss, State::GotStartup) |
            (Auth::Sspi, State::GotStartup) =>
                Ok(State::AskedGssResponse),
            (Auth::GssContinue {..}, State::GotGssResponse) =>
                Ok(State::AskedGssResponse),
            (Auth::Md5Password {..}, State::GotStartup) =>
                Ok(State::AskedMd5Password),
            (Auth::Ok, State::FinishedSasl) |
            (Auth::Ok, State::GotCleartextPassword) |
            (Auth::Ok, State::GotGssResponse) |
            (Auth::Ok, State::GotMd5Password) |
            (Auth::Ok, State::GotStartup) =>
                Ok(State::Authenticated),
            (Auth::KerberosV5, State::GotStartup) =>
                Err(Unsupported(
                    "AuthenticationKerberosV5 is unsupported after PostgreSQL 9.3 \
                    which in turn is unsupported by PostgreSQL maintainers"
                )),
            (Auth::Sasl {..}, State::GotStartup) =>
                Ok(State::AskedSaslInitialResponse),
            (Auth::SaslContinue {..}, State::GotAnySaslResponse) =>
                Ok(State::AskedSaslResponse),
            (Auth::SaslFinal {..}, State::GotAnySaslResponse) =>
                Ok(State::FinishedSasl),
            (Auth::ScmCredential, State::GotStartup) =>
                Err(Unsupported(
                    "This message type is only issued by pre-9.1 servers. \
                    It may eventually be removed from the protocol specification."
                )),
            (_, State::GotStartup) =>
                Err(Todo("Authentication::* is not fully implemented yet".into())),
            _ =>
                Err(UnexpectedType(state, Backend, type_byte)),
        }
    }

    // util:

    async fn read_backend_through<Msg>(&mut self) -> ConveyResult<Msg>
    where
        Msg: 'static + MsgDecode + Send + marker::Sync,
    {
        let (bytes, msg) = map_tls_read_result(self.backend.read_msg::<Msg>().await)?;
        (self.callback)(Backend, &msg);
        write(&mut self.frontend, &bytes).await?;
        Ok(msg)
    }

    async fn read_frontend_through<Msg>(&mut self) -> ConveyResult<Msg>
    where
        Msg: 'static + MsgDecode + Send + marker::Sync,
    {
        let (bytes, msg) = map_tls_read_result(self.frontend.read_msg::<Msg>().await)?;
        (self.callback)(Frontend, &msg);
        write(&mut self.backend, &bytes).await?;
        Ok(msg)
    }

    async fn read_type_byte_from_both(&mut self) -> ConveyResult<(Side, TypeByte)> {
        let either = future::select(
            read_type_byte(&mut self.backend).boxed(),
            read_type_byte(&mut self.frontend).boxed(),
        ).await;
        // TODO: if both futures are ready, do we loose a result of the second one?
        let (side, result) = match either {
            Either::Left((backend, _frontend)) => (Backend, backend),
            Either::Right((frontend, _backend)) => (Frontend, frontend),
        };
        let byte = result?;
        let type_byte = TypeByte::try_from(byte).map_err(|_| UnknownType(side, byte))?;
        Ok((side, type_byte))
    }

    async fn switch_frontend_to_tls(&mut self) -> Result<(), Error> {
        let frontend_tls_server = &self.frontend_tls_server;
        self.frontend.establish_tls(|plain| { frontend_tls_server.accept(plain) }).await
            .map_err(TlsError)
    }

    async fn switch_backend_to_tls(&mut self) -> Result<(), Error> {
        let backend_tls_client = &self.backend_tls_client;
        self.backend.establish_tls(|plain| { backend_tls_client.connect(plain) }).await
            .map_err(TlsError)
    }
}

async fn read_type_byte<Plain, Tls>(tlsable: &mut Tlsable<Plain, Tls>) -> ConveyResult<u8>
where Plain: Reader, Tls: Reader {
    map_tls_io_result(tlsable.read_type_byte().await)
}

async fn write<Plain, Tls>(tlsable: &mut Tlsable<Plain, Tls>, bytes: &[u8]) -> ConveyResult<()>
where Plain: Writer, Tls: Writer {
    map_tls_io_result(tlsable.write(bytes).await)
}

fn map_tls_io_result<T>(tls_result: TlsResult<IoResult<T>>) -> ConveyResult<T> {
    tls_result.map_err(TlsError)?.map_err(IoError)
}

fn map_tls_read_result<Msg>(read_result: TlsResult<ReadResult<Msg>>) -> ConveyResult<(Vec<u8>, Msg)> {
    let ReadData { bytes, msg_result } = read_result.map_err(TlsError)?.map_err(map_read_error)?;
    let msg = msg_result.map_err(map_msg_error)?;
    Ok((bytes, msg))
}

fn map_read_error(read_error: ReadError) -> Error {
    match read_error {
        ReadError::IoError(io_error) => IoError(io_error),
        ReadError::EncodeError(encode_error) => EncodeError(encode_error),
    }
}

fn map_msg_error(msg_error: MsgError) -> Error {
    match msg_error {
        MsgError::DecodeError(decode_error) => DecodeError(decode_error),
        MsgError::LeftUndecoded(left) => LeftUndecoded(left),
    }
}

const TLS_SUPPORTED: u8 = b'S';
const TLS_NOT_SUPPORTED: u8 = b'N';
